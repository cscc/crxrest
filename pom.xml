<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<!-- coordinates, name, other meta info -->
	<groupId>edu.unc.cscc.crxrest</groupId>
	<artifactId>crxrest</artifactId>
	<version>1.0.0-SNAPSHOT</version>

	<name>crxrest</name>
	<inceptionYear>2017</inceptionYear>
	<description>CRxREST - a multi-database medication lookup and coding system</description>
	<url>https://gitlab.com/cscc/crxrest</url>

	<licenses>
		<license>
			<name>3-Clause BSD License</name>
			<url>https://opensource.org/licenses/BSD-3-Clause</url>
		</license>
	</licenses>

	<organization>
		<name>CSCC, University of North Carolina</name>
		<url>http://www.cscc.unc.edu/</url>
	</organization>

	<developers>
		<developer>
			<id>rob</id>
			<name>Rob Tomsick</name>
			<email>rtomsick@unc.edu</email>
			<organization>${project.organization.name}</organization>
			<organizationUrl>${project.organization.url}</organizationUrl>
			<roles>
				<role>architect</role>
				<role>developer</role>
			</roles>
			<timezone>America/New_York</timezone>
		</developer>
	</developers>


	<!-- on to the useful stuff -->

	<packaging>pom</packaging>


	<properties>
		<jackson.version>2.9.0</jackson.version>
		<swagger.version>1.5.16</swagger.version>
		<java.version>11</java.version>
	</properties>

	<modules>
		<module>model</module>
		<module>app</module>
		<module>ndc-dictionary</module>
		<module>simple-dictionaries</module>
	</modules>

	<build>
		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.codehaus.mojo</groupId>
					<artifactId>license-maven-plugin</artifactId>
					<version>1.12</version>
					<configuration>
						<verbose>false</verbose>
						<addSvnKeyWords>false</addSvnKeyWords>
						<inceptionYear>2017</inceptionYear>
						<organizationName>CSCC - University of North Carolina</organizationName>
						<licenseName>bsd_3</licenseName>
						<includes>
							<include>**/*.java</include>
						</includes>
						<roots>
							<root>src/main/java</root>
						</roots>

						<addJavaLicenseAfterPackage>false</addJavaLicenseAfterPackage>
						<processStartTag>========================LICENSE_START=================================</processStartTag>
						<processEndTag>=========================LICENSE_END==================================</processEndTag>
					</configuration>
					<executions>
						<execution>
							<id>first</id>
							<goals>
								<goal>update-file-header</goal>
								<goal>download-licenses</goal>
							</goals>
							<phase>process-sources</phase>
						</execution>
						<!-- this is bound to the compile phase. Why? Because under whatever 
							environment bitbucket's pipelines uses, binding to anything before that seems 
							to confuse the reactor and cause it to be unable to find modules used as 
							dependencies elsewhere in this project. -->
						<execution>
							<id>second</id>
							<goals>
								<goal>aggregate-add-third-party</goal>
							</goals>
							<phase>compile</phase>
							<configuration>
								<outputDirectory>${project.build.outputDirectory}/META-INF/</outputDirectory>
							</configuration>
						</execution>
					</executions>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>com.fasterxml.jackson.core</groupId>
				<artifactId>jackson-core</artifactId>
				<version>${jackson.version}</version>
			</dependency>
			<dependency>
				<groupId>com.fasterxml.jackson.core</groupId>
				<artifactId>jackson-annotations</artifactId>
				<version>${jackson.version}</version>
			</dependency>
			<dependency>
				<groupId>com.fasterxml.jackson.core</groupId>
				<artifactId>jackson-databind</artifactId>
				<version>${jackson.version}</version>
			</dependency>
			<dependency>
				<groupId>org.apache.commons</groupId>
				<artifactId>commons-lang3</artifactId>
				<version>3.5</version>
			</dependency>
			
			<dependency>
				<groupId>javax.validation</groupId>
				<artifactId>validation-api</artifactId>
				<version>2.0.1.Final</version>
				<scope>provided</scope>
			</dependency>

			<dependency>
				<groupId>edu.unc.cscc.crxrest</groupId>
				<artifactId>model</artifactId>
				<version>${project.version}</version>
			</dependency>

			<dependency>
				<groupId>org.slf4j</groupId>
				<artifactId>slf4j-api</artifactId>
				<version>1.7.24</version>
			</dependency>

			<dependency>
				<groupId>com.h2database</groupId>
				<artifactId>h2</artifactId>
				<version>1.4.196</version>
				<scope>provided</scope>
			</dependency>

			<dependency>
				<groupId>org.xerial</groupId>
				<artifactId>sqlite-jdbc</artifactId>
				<version>3.21.0.1</version>
				<scope>provided</scope>
			</dependency>

			<dependency>
				<groupId>com.fasterxml.uuid</groupId>
				<artifactId>java-uuid-generator</artifactId>
				<version>3.1.3</version>
			</dependency>

			<dependency>
				<groupId>org.jooq</groupId>
				<artifactId>jooq</artifactId>
				<version>3.10.7</version>
			</dependency>
			<dependency>
                 <groupId>javax.xml.bind</groupId>
                 <artifactId>jaxb-api</artifactId>
                 <version>2.3.1</version>
             </dependency>


			<dependency>
				<groupId>io.swagger</groupId>
				<artifactId>swagger-core</artifactId>
				<scope>compile</scope>
				<version>${swagger.version}</version>
				<exclusions>
					<exclusion>
						<groupId>javax.ws.rs</groupId>
						<artifactId>jsr311-api</artifactId>
					</exclusion>
				</exclusions>
			</dependency>
		</dependencies>
	</dependencyManagement>

</project>
