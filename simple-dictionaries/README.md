simple-dictionaries
===

`simple-dictionaries` is a module of a set of dictionary providers designed to provide
a variety of dictionaries that utilize a much simplier set of rules.

May implement or extend the 'Simple' object to allow returning 'SimpleResults'


Building
---

`simple-dictionaries` will be built as part of the `crxrest` project's build
cycle.  The data files are **not** required to build the project, as they
are only used at run time.

Usage
---

This dictionary provider requires data files for each of the providers included, specifically the 
`ocd.csv` file and 'authors.txt'.  On startup the provider will attempt to load each file
from the path specified by the system property  `crxrest.providers.ocd.data-path` and `crxrest.providers.authors.data-path`.
If this property is not specified, the provider will attempt to load the files from 
`/usr/local/share/crxrest/ocd/`


java -jar ./crxrest/app/target/app-1.0.0-SNAPSHOT.jar \
--crxrest.providers.config.crx-ocd.data-path=./crxrest/simple-dictionaries/ocd.csv \
--crxrest.providers.config.crx-authors.data-path=./crxrest/simple-dictionaries/authors.csv \
--crxrest.providers.paths="./crxrest/simple-dictionaries/target/simple-dictionaries-1.0.0-SNAPSHOT-jar-with-dependencies.jar" \
--server.port=8085

