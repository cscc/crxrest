/*-
 * ========================LICENSE_START=================================
 * ocd-dictionary
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.crxrest.providers.ocd;

import static edu.unc.cscc.crxrest.providers.ocd.OCDServiceProvider.O_TABLE;
import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.constraints.NotNull;

import org.jooq.DSLContext;
import org.jooq.Field;

import edu.unc.cscc.crxrest.Dictionary;
import edu.unc.cscc.crxrest.DictionaryService;
import edu.unc.cscc.crxrest.model.Drug;
import edu.unc.cscc.crxrest.model.NDC;
import edu.unc.cscc.crxrest.model.Simple;
import edu.unc.cscc.crxrest.model.TradeName;
import edu.unc.cscc.crxrest.model.barcode.Barcode;
import edu.unc.cscc.crxrest.model.product.Product;
import me.xdrop.fuzzywuzzy.FuzzySearch;
import me.xdrop.fuzzywuzzy.model.ExtractedResult;


public class OCDDictionaryService
implements DictionaryService
{
	public static final int			MIN_QUERY_LEN = 2;

	private static final int		RESULT_LIMIT = 46;
	private static final int 		SCORE_THRESHOLD = 50;
	
	private final DSLContext		ctx;
	private final Dictionary		dictionary;
	
	OCDDictionaryService(DSLContext ctx, Dictionary dictionary)
	{
		this.ctx = ctx;
		this.dictionary = dictionary;
	}

	@Override
	public int
	resultLimit()
	{
		return RESULT_LIMIT;
	}

	@Override
	public Dictionary
	dictionary()
	{
		return this.dictionary;
	}

	@Override
	public List<Simple> 
	findByName(final String query, int limit)
	{
		limit = limit > 0 ? limit : this.resultLimit();

		if (query.length() < MIN_QUERY_LEN)
		{
			return Collections.emptyList();
		}
		
		final String term = query.trim();
		
		final Field<String> nf = field(name("occupation"), String.class);
		final Field<Integer> cf = field(name("census_code"), Integer.class);
		final Field<Integer> sf = field(name("prestige_score"), Integer.class);
		
		final List<Occupation> occupations = this.ctx
				.selectFrom(O_TABLE)
				.fetch(r -> new Occupation(r.get(nf), r.get(cf), r.get(sf)))
				.stream()
				.collect(Collectors.toCollection(ArrayList :: new));
		 
		
		List<ExtractedResult> fuzzyResults = FuzzySearch.extractSorted(term,
				occupations.stream().map(Occupation::name).collect(Collectors.toList()));
		
		List<Simple> finalResults = fuzzyResults.stream()
				.filter(r -> r.getScore() > SCORE_THRESHOLD)
				.limit(limit)
				.map(r -> occupations.get(r.getIndex()))
				.collect(Collectors.toCollection(ArrayList::new));
		
		// Adding 4 special options to the end
		// TODO -- Should we consider these DB entries to all be String, so that we can include empty data, instead of being forced to pass '0'?
		Occupation uncodable = new Occupation("Uncodable", 9997, 0);
		Occupation dontKnow = new Occupation("Don't Know", 9998, 0);
		Occupation noAnswer = new Occupation("No Answer", 9999, 0);
		Occupation notApplicable = new Occupation("Not applicable", 0, 0);
		
		finalResults.add(uncodable);
		finalResults.add(dontKnow);
		finalResults.add(noAnswer);
		finalResults.add(notApplicable);
		
		return finalResults;
	}




	@Override
	public @NotNull List<TradeName> findTradeNames(String query, int limit) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public @NotNull List<Drug> findDrugs(String query, int limit) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public @NotNull List<Product> findProducts(String query, int limit) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Product> findByBarcode(Barcode<?> barcode, int limit) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public @NotNull List<Product> findByNDC(NDC ndc, int limit) {
		// TODO Auto-generated method stub
		return null;
	}


}
