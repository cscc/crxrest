/*-
 * ========================LICENSE_START=================================
 * authors
 * %%
 * Copyright (C) 2024 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.crxrest.providers.authors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.jooq.DSLContext;
import org.jooq.Field;

import edu.unc.cscc.crxrest.Dictionary;
import edu.unc.cscc.crxrest.DictionaryService;
import edu.unc.cscc.crxrest.model.Drug;
import edu.unc.cscc.crxrest.model.NDC;
import edu.unc.cscc.crxrest.model.Simple;
import edu.unc.cscc.crxrest.model.TradeName;
import edu.unc.cscc.crxrest.model.barcode.Barcode;
import edu.unc.cscc.crxrest.model.product.Product;

import me.xdrop.fuzzywuzzy.FuzzySearch;
import me.xdrop.fuzzywuzzy.model.ExtractedResult;

import static edu.unc.cscc.crxrest.providers.authors.AuthorsServiceProvider.A_TABLE;
import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;


public class AuthorsDictionaryService
implements DictionaryService
{
	public static final int			MIN_QUERY_LEN = 2;

	private static final int		RESULT_LIMIT = 1000;	
	
	private final DSLContext		ctx;
	private final Dictionary		dictionary;
	
	AuthorsDictionaryService(DSLContext ctx, Dictionary dictionary)
	{
		this.ctx = ctx;
		this.dictionary = dictionary;
	}

	@Override
	public int
	resultLimit()
	{
		return RESULT_LIMIT;
	}

	@Override
	public Dictionary
	dictionary()
	{
		return this.dictionary;
	}

	@Override
	public List<TradeName> 
	findTradeNames(final String query, int limit)
	{
		throw new UnsupportedOperationException("Not handled by this implementation.");		
	}

	@Override
	public List<Drug> 
	findDrugs(final String query, int limit)
	{
		throw new UnsupportedOperationException("Not handled by this implementation.");
	}

	@Override
	public List<Product> 
	findProducts(final String query, int limit)
	{
		throw new UnsupportedOperationException("Not handled by this implementation.");
	}

	@Override
	public List<Product> 
	findByNDC(final NDC ndc, int limit)
	{
		throw new UnsupportedOperationException("Not handled by this implementation.");
	}

	@Override
	public List<Product> 
	findByBarcode(Barcode<?> barcode, int limit)
	{
		throw new UnsupportedOperationException("Not handled by this implementation.");
	}
	
	@Override
	public List<Simple>
	findByName(final String query, int limit)
	{
		limit = limit > 0 ? limit : this.resultLimit();

		if (query.length() < MIN_QUERY_LEN)
		{
			return Collections.emptyList();
		}
		
		final String term = query.trim();
		
		final Field<String> af = field(name("author"), String.class);
		final Field<Integer> an = field(name("num"), Integer.class);
		final Field<String> ai = field(name("institution"), String.class);
		
		final List<Simple> authors = this.ctx
				.selectFrom(A_TABLE)
				.fetch(r -> new Author(r.get(af), r.get(an), r.get(ai)))
				.stream()
				.collect(Collectors.toCollection(ArrayList :: new));
		
		List<ExtractedResult> fuzzyResults = FuzzySearch.extractSorted(term,
				authors.stream().map(Simple::name).collect(Collectors.toList()));
		
		List<Simple> finalResults = fuzzyResults.stream()
				.filter(r -> r.getScore() > 50)
				.limit(limit)
				.map(r -> authors.get(r.getIndex()))
				.collect(Collectors.toCollection(ArrayList::new));
		
		return finalResults;
	}
	
}
