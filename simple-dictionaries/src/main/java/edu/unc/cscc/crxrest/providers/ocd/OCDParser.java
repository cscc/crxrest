/*-
 * ========================LICENSE_START=================================
 * ocd-dictionary
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.crxrest.providers.ocd;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Stream;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;

import edu.unc.cscc.crxrest.providers.ocd.OCDParser.Entry;

/**
 * Parser for occupation data files.
 * 
 * Format documentation available from 
 * <a href="https://gss.norc.org/documents/codebook/GSS_Codebook_AppendixF.pdf">norc.org</a>
 * 
 *
 */
public final class OCDParser
implements Iterable<Entry>, Closeable
{
	private static final String[]	colNames = {
			"Occupation","2010 Census Code","2010 Prestige Score"
	};
	
	private final InputStream		in;
	private final BufferedReader	reader;
	
	private CSVParser				parser;
	private Iterator<CSVRecord>		lineIter;
	
	private OCDParser(InputStream in, Charset cs)
	{
		this.in = in;
		this.reader = new BufferedReader(new InputStreamReader(in, cs));
	}
	
	/**
	 * Read and parse the next entry from the backing input.
	 * 
	 * @return next entry or <code>null</code> if the input has been exhausted
	 * @throws IOException thrown if an error occurred while reading the next
	 * line of the input
	 * @throws IllegalArgumentException thrown if the read line could not be 
	 * parsed
	 */
	public Entry
	next()
	throws IOException
	{
		CSVRecord r;
		
		try
		{
			r = this.lineIter.next();
		}
		catch (NoSuchElementException e)
		{
			return null;
		}
		
		return new Entry(r.get(colNames[0]), 
				StringUtils.isNumeric(r.get(colNames[1]))?Integer.parseInt(r.get(colNames[1])):-1, 
				StringUtils.isNumeric(r.get(colNames[2]))?Integer.parseInt(r.get(colNames[2])):-1);
	}
	
	/**
	 * Whether the underlying input has any lines which have not been parsed.
	 * 
	 * @return <code>true</code> if the input has lines which have not been 
	 * parsed, <code>false</code> otherwise
	 */
	public boolean
	hasNext()
	{
		return this.lineIter.hasNext();
	}
	
	/**
	 * Initialize the parser.
	 * 
	 * @throws IOException thrown if initialization failed
	 */
	public void
	init()
	throws IOException
	{
		if (this.parser == null)
		{
			this.parser =  CSVFormat.DEFAULT.withHeader().parse(this.reader);
			this.lineIter = this.parser.iterator();
			
			/* check that the header contains sane things */
			final Map<String, Integer> headerMap = this.parser.getHeaderMap();
			final boolean valid = 
					Stream.of(colNames).allMatch(headerMap :: containsKey);
			
			if (! valid)
			{
				throw new IOException("Unrecognized header");	
			}
		}
	}
	
	
	@Override
	public Iterator<Entry>
	iterator()
	{
		return new Iterator<Entry>()
		{

			@Override
			public boolean
			hasNext()
			{
				return OCDParser.this.hasNext();
			}

			@Override
			public Entry next()
			{
				try
				{
					return OCDParser.this.next();
				}
				catch (IOException e)
				{
					throw new RuntimeException(e);
				}
			}
			
		};
	}
	
	/**
	 * Release the resources held by the parser.  This will close any 
	 * input streams provided to the parser at the time of creation, as well
	 * as any readers or other resources used internally.
	 * 
	 * @throws IOException 
	 */
	@Override
	public void
	close()
	throws IOException
	{
		if (this.parser != null)
		{
			this.parser.close();
		}
		this.reader.close();
		this.in.close();
	}

	/**
	 * Create a parser which will consume the given input stream.
	 * 
	 * @param in stream to consume
	 * @return parser
	 * @throws IOEXception thrown if the parser could not be initialized
	 */
	public static final OCDParser
	create(InputStream in) throws IOException
	{
		return create(in, Charset.defaultCharset());
	}
	
	/**
	 * Create a parser which will consume the given input stream, interpreting
	 * it according to the rules of the given character set.
	 * 
	 * @param in stream to consume
	 * @param cs character set against which to interpret stream
	 * @return parser
	 * @throws IOException thrown if the parser could not be initialized
	 */
	public static final OCDParser
	create(InputStream in, Charset cs)
	throws IOException
	{
		OCDParser parser;
		
		if (in instanceof BufferedInputStream)
		{
			parser = new OCDParser(in, cs);
		}
		
		parser = new OCDParser(new BufferedInputStream(in, 16 * 1024), cs);
		
		parser.init();
		
		return parser;
	}
	
	
	public static final class Entry
	{
		private final String		occupation;
		private final int			censusCode;
		private final int			prestigeScore;
		
		public final String getOccupation() {
			return occupation;
		}

		public final int getCensusCode() {
			return censusCode;
		}

		public final int getPrestigeScore() {
			return prestigeScore;
		}

		private Entry(String occupation, int censusCode,
				int prestigeScore)
		{
			this.occupation = occupation;
			this.censusCode = censusCode;
			this.prestigeScore = prestigeScore;
		}
	}
}
