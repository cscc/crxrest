/*-
 * ========================LICENSE_START=================================
 * ocd-dictionary
 * %%
 * Copyright (C) 2017 - 2024 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.crxrest.providers.ocd;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;

import javax.sql.DataSource;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.Table;
import org.jooq.conf.RenderNameStyle;
import org.jooq.conf.Settings;
import org.jooq.impl.DSL;
import org.jooq.impl.DefaultDSLContext;

import com.fasterxml.uuid.NoArgGenerator;
import com.fasterxml.uuid.impl.RandomBasedGenerator;

import edu.unc.cscc.crxrest.ConfigurableProvider;
import edu.unc.cscc.crxrest.DBDictionaryProvider;
import edu.unc.cscc.crxrest.DictionaryService;
import edu.unc.cscc.crxrest.Discoverable;

@Discoverable
public class OCDServiceProvider
implements DBDictionaryProvider, ConfigurableProvider
{
	public static final String             IDENTIFIER = "OCD";
	public static final String             DEFAULT_PATH = 
			"/usr/local/share/crxrest/ocd/ocd.csv";
	
	static final Table<?>                  O_TABLE = 
			DSL.table(DSL.name("occupation"));
	
	private final NoArgGenerator           uuidGenerator = 
	        new RandomBasedGenerator(null);
	
	private String                         productFilePath;
	
	private DictionaryService              ocdService;
	
	@Override
	public String persistenceID()
	{
		return "DB-" + this.getClass().getCanonicalName();
	}
	
	@Override
	public void 
	initialize(DataSource ds, boolean newDS)
	throws IOException
	{
		/* set up our database */
		final DSLContext ctx = this.initializeDB(ds);
		
		if (! newDS)
		{
			/* set up service */
			this.ocdService = new OCDDictionaryService(ctx, 
											new OCDDictionary(IDENTIFIER, "persistent"));
			return;
		}

		/* see if we have a search path defined */
		
		if (this.productFilePath == null)
		{
			throw new IOException("No dictionary file path specified");
		}
		
		final File f = new File(this.productFilePath);
		
		if (! f.canRead())
		{
			throw new IOException(String.format(
					"Unable to read dictionary file at '%s'", 
					f.getCanonicalPath()));
		}
		
		/* set up our parser */
		OCDParser parser = OCDParser.create(new FileInputStream(f));	
		
		ctx.transaction(config -> {
			for (final OCDParser.Entry e : parser)
			{				
				final UUID id = this.uuidGenerator.generate();
				
				DSL.using(config)
					.insertInto(O_TABLE)
					.set(field(name("id"), UUID.class), id)
					.set(field(name("occupation"), String.class), 
							e.getOccupation())
					.set(field(name("census_code"), Integer.class), 
							e.getCensusCode())
					.set(field(name("prestige_score"), Integer.class), 
							e.getPrestigeScore())
					.execute();
			}
		});
		
		parser.close();
		
		/* set up service */
		this.ocdService = new OCDDictionaryService(ctx, 
										new OCDDictionary(IDENTIFIER, productFilePath));
	}

	@Override
	public Set<String>
	providedDictionaries()
	{
		return Collections.singleton(IDENTIFIER);
	}

	@Override
	public DictionaryService 
	load(String identifier)
	{
		if (this.ocdService == null)
		{
			throw new RuntimeException(
					"Cannot provide service; provider was not inititalized.  "
					+ "This is a bug in the calling code (CRxRest application).");
		}
		
		if (IDENTIFIER.equals(identifier))
		{
			return this.ocdService;
		}
		
		return null;
	}
	
	private DSLContext
	initializeDB(DataSource ds)
	{
		Settings settings = new Settings();
		SQLDialect dialect = SQLDialect.SQLITE;
		settings.withRenderNameStyle(RenderNameStyle.QUOTED);
		
		DSLContext ctx = new DefaultDSLContext(ds, dialect, settings);
		
		return ctx;
	}

	@Override
	public InputStream
	schemaStream() 
	throws IOException
	{
		return this.getClass()
					.getClassLoader()
					.getResourceAsStream("edu/unc/cscc/crxrest/providers/ocd/schema.sql");
	}
	
	@Override
	public @NotNull @NotBlank String 
	configurationIdentifier()
	{
	    return "crx-ocd";
	}

    @Override
    public void 
    setProperties(Properties properties)
    {
        this.productFilePath = properties.getProperty("data-path");
        
        if (this.productFilePath == null)
        {
            this.productFilePath = DEFAULT_PATH;
        }
    }
}
