/*-
 * ========================LICENSE_START=================================
 * ocd-dictionary
 * %%
 * Copyright (C) 2017 - 2024 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.crxrest.providers.ocd;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonGetter;

import edu.unc.cscc.crxrest.model.Simple;

import io.swagger.annotations.ApiModelProperty;

public class Occupation extends Simple {
	
	private final String name;
	private final int code;
	private final int score;
	
	public Occupation(String name, int code, int score) {
		super(name);
		this.name = name;
		this.code = code;
		this.score = score;
	}
	
	/**
	 * The displayable name of the product. User-facing and free-form.
	 * 
	 * @return display name, not <code>null</code>, not empty
	 */
	@NotNull
	@Size(min = 1)
	@JsonGetter("name")
	@ApiModelProperty(value = "The displayable name of the occupation. User-facing "
							+ "and usually the same as the profession name.",
					example = "Computer Programmer")
	public String name() {
		return this.name;
	}
	
	/**
	 * The 2010 census code for the profession.
	 * 
	 * @return census code, not <code>null</code>, not empty
	 */
	@NotNull
	@Size(min = 1)
	@JsonGetter("code")
	@ApiModelProperty(value = "The census code.", example = "4610")
	public int code() {
		return this.code;
	}
		
	/**
	 * The profession prestige score.
	 * 
	 * @return prestige score, not <code>null</code> not empty.
	 */
	@JsonGetter("score")
	@ApiModelProperty(value = "The Profession's prestige score.", example = "56")
	public int score() {
		return this.score;
	}
	
	/**
	 * Get the display name that should be used when presenting this product
	 * to a user.  By default, this delegates to {@link #name()}.
	 * 
	 * @return display name, not <code>null</code>
	 */
	@Override
	@JsonGetter("display-name")
	@ApiModelProperty(value = "A human-readable name for use when presenting "
							+ "the occupation to a user, usually an alias for "
							+ "the 'name' property.",
					  example = "Computer Programmer")
	public String displayName() {
		return this.name();
	}

}
