DROP TABLE IF EXISTS "authors";

CREATE TABLE "authors" (
	"id"				UUID PRIMARY KEY NOT NULL,
	"author"			VARCHAR(256) NOT NULL,
	"num"				INTEGER NOT NULL,
	"institution"		VARCHAR(256)
);

CREATE INDEX IF NOT EXISTS "author_n_idx" ON "authors" ("author");