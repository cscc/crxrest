DROP TABLE IF EXISTS "occupation";

CREATE TABLE "occupation" (
	"id"					UUID PRIMARY KEY NOT NULL,
	"occupation"			VARCHAR(256) NOT NULL,
	"census_code"			INTEGER NOT NULL,
	"prestige_score"		INTEGER NOT NULL
);

CREATE INDEX IF NOT EXISTS "occupation_n_idx" ON "occupation" ("occupation");
CREATE INDEX IF NOT EXISTS "occupation_cs_idx" ON "occupation" ("census_code");
CREATE INDEX IF NOT EXISTS "prestige_score_cs_idx" ON "occupation" ("prestige_score");
