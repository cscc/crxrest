package edu.unc.cscc.crxrest.test.model;

import static org.junit.Assert.fail;

import java.util.function.BiConsumer;

import org.junit.Assert;
import org.junit.Test;

import edu.unc.cscc.crxrest.model.NDC;

public class NDCTest
{
	

	@Test
	public final void
	badParse()
	{
		
		final String[] bad = new String[]{
				"8937101-1844-11",
				"837-1233-1",
				"8234798234hn",
				"12345-6789-101"
			};
		
		for (String str : bad)
		{
			try
			{
				NDC.parse(str);
			}
			catch(IllegalArgumentException e)
			{
				continue;
			}
			fail("accepted malformed NDC: " + str);
		}
	}
	
	@Test
	public final void
	parseFull()
	{
		final String[] inputs = new String[] {
				"58271-4728-12", /* 5-4-2 */
				"59382-2348-1", /* 5-4-1 */
				"59382-291-01", /* 5-3-2 */
				"4928-2817-13" /* 4-4-2 */
			};
			
		for (final String str : inputs)
		{
			final NDC ndc = NDC.parse(str);
			
			Assert.assertEquals(str, ndc.toString());
		}
	}
	
	@Test
	public final void
	parsePartial()
	{
		final String[] inputs = new String[] {
			"59382-2348", /* 5-4 */
			"59382-291", /* 5-3 */
			"4928-2817" /* 4-4 */
		};
		
		for (final String str : inputs)
		{
			final NDC ndc = NDC.parse(str);
			
			Assert.assertEquals(str, ndc.toString());
		}
	}
	
	@Test
	public final void
	testNormalization()
	{
		BiConsumer<String, String> test = 
				(t, e) -> Assert.assertEquals(e, NDC.parse(t).normalize().toString());
		
		/* 4-4-2 -> 5-4-2 */
		test.accept("0002-7597-01", "00002-7597-01");
		/* 5-3-2 -> 5-4-2 */
		test.accept("50242-040-62", "50242-0040-62");
		/* 5-4-1 -> 5-4-2 */
		test.accept("60575-4112-1", "60575-4112-01");

		/* partial NDC normalization */
		test.accept("50242-040", "50242-0040");
		test.accept("0002-7597", "00002-7597");
		test.accept("60575-4112", "60575-4112");
	}

	@Test
	public final void 
	createSegmented()
	{
		
		/* 5-3-2 */
		NDC ndc = new NDC(
				new int[]{5,1,6,2,4},
				new int[]{1,1,0},
				new int[]{9,1});
		Assert.assertEquals("51624-110-91", ndc.toString());
		
		/* 4-4-2 */
		ndc = new NDC(
				new int[]{1,6,2,4},
				new int[]{9,1,1,0},
				new int[]{9,1});
		Assert.assertEquals("1624-9110-91", ndc.toString());
		
		/* 5-4-1 */
		ndc = new NDC(
				new int[]{5,1,6,2,4},
				new int[]{9,1,1,0},
				new int[]{1});
		Assert.assertEquals("51624-9110-1", ndc.toString());
		
		/* 5-4-2 */
		ndc = new NDC(
				new int[]{5,1,6,2,4},
				new int[]{9,1,1,0},
				new int[]{9,1});
		Assert.assertEquals("51624-9110-91", ndc.toString());
	}

	@Test
	public final void 
	create11Digit()
	{
		NDC ndc = NDC.create(new int[] {4,9,8,7,2,0,2,3,4,1,1});
		
		Assert.assertEquals("49872-0234-11", ndc.toString());
	}

}
