/*-
 * ========================LICENSE_START=================================
 * model
 * %%
 * Copyright (C) 2017 - 2018 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.crxrest;

import java.util.Properties;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public interface ConfigurableProvider 
extends DictionaryProvider
{

    /**
     * A unique identifier of the provider, used for instance-managed 
     * configuration.
     * 
     * 
     * @return identifier, not <code>null</code>, not blank
     */
    @NotNull
    @NotBlank
    String configurationIdentifier();
    
    /**
     * <p>
     * Set the properties configured for this provider.  The key space of the 
     * given properties will be any detected properties <i>beneath</i> the 
     * {@link #configurationIdentifier() configuration identifier}, stripped of 
     * their prefix(es).  So for example, a service with the identifier 
     * {@code foo-svc} when configured with a property 
     * {@code foo-svc.initialize=true} would see a property entry of 
     * {@code initialize} with a value of <code>true</code>.
     * </p>
     * 
     * <p>
     * This method will only be invoked once, prior to initialization.
     * </p>
     * 
     * @param properties properties, not <code>null</code>
     */
    void setProperties(Properties properties);
}
