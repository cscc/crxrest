/*-
 * ========================LICENSE_START=================================
 * model
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.crxrest.model.product;

import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonTypeName;

import edu.unc.cscc.crxrest.Dictionary;
import edu.unc.cscc.crxrest.model.NDC;
import edu.unc.cscc.crxrest.model.barcode.Barcode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * A pharmaceutical product.  A product consists of one or more components
 * in specific quantities, as well as an NDC (if applicable.) There
 * is a logical assumption that a product refers to a single dispensible
 * unit, thus all quantities for its components are assumed to be those 
 * present in a single dose of the product as specified by the supplier.
 * </p>
 * 
 * <p>
 * Note that a product does not necessarily correspond to an individual 
 * retail SKU.  The relationship between products and their SKU(s) is not
 * captured by this model; instead all known barcodes corresponding to a 
 * product are considered to be associated with the product even though they
 * may be distinct from a retail perspective.
 * </p>
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
@JsonTypeName("product")
@ApiModel(value = "product",
		description = "A pharmaceutical product.  A product consists of one "
					+ "or more components in specific quantities, as well as "
					+ "an NDC (if applicable.) There is a logical assumption "
					+ "that a product refers to a single dispensible unit, "
					+ "thus all quantities for its components are assumed to "
					+ "be those present in a single dose of the product as "
					+ "specified by the supplier. Note that a product does "
					+ "not necessarily correspond to an individual retail SKU")
public interface Product
{
	/**
	 * The displayable name of the product. User-facing and free-form.
	 * 
	 * @return display name, not <code>null</code>, not empty
	 */
	@NotNull
	@Size(min = 1)
	@JsonGetter("name")
	@ApiModelProperty(value = "The displayable name of the product. User-facing "
							+ "and free-form.",
					example = "Equate Ibuprofen Tablets, 200 mg")
	public String name();
	
	/**
	 * The components which make up the product.  All products must contain
	 * at least one component.
	 * 
	 * @return components of the product, not <code>null</code>, not empty
	 */
	@NotNull
	@Size(min = 1)
	@JsonGetter("components")
	@ApiModelProperty(value = "The components which make up the product.")
	public Set<ProductComponent> components();
	
	/**
	 * The NDC of the product.  May not be present if product information was 
	 * obtained from a {@link Dictionary dictionary} which does not support 
	 * NDCs.
	 * 
	 * @return NDC of the product (if known), <code>null</code> otherwise
	 */
	@JsonGetter("ndc")
	@ApiModelProperty(value = "The NDC of the product.  May be null if not "
							+ "supported by source dictionary.",
						example = "49035-248-05", dataType = "string")
	public NDC ndc();
	
	/**
	 * The set of all known barcodes associated with the product (if supported
	 * by the dictionary from which the product was obtained.)
	 * 
	 * @return barcodes, or <code>null</code> if barcode information is not 
	 * supplied by the source dictionary
	 */
	@JsonGetter("barcodes")
	@ApiModelProperty(value = "The set of all known barcodes associated with "
							+ "the product (if support by the source "
							+ "dictionary), or null if barcode information is "
							+ "not supplied by the source dictionary.")
	public Set<Barcode<?>> barcodes();
	
	/**
	 * Get the display name that should be used when presenting this product
	 * to a user.  By default, this delegates to {@link #name()}.
	 * 
	 * @return display name, not <code>null</code>
	 */
	@JsonGetter("display-name")
	@ApiModelProperty(value = "A human-readable name for use when presenting "
							+ "the product to a user, usually an alias for "
							+ "the 'name' property.",
						example = "Equate Ibuprofen Tablets, 200 mg")
	public default String 
	displayName()
	{
		return this.name();
	}
}
