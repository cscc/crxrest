/*-
 * ========================LICENSE_START=================================
 * model
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.crxrest.model;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;

import edu.unc.cscc.crxrest.model.product.Product;
import edu.unc.cscc.crxrest.model.product.ProductComponent;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * A unit of dosage.  A dose is comprised of two parts: a unit of 
 * measurement, and a finite, non-negative quantity (implied to be of that
 * unit).  A dose does not necessarily refer to a specific quantity for a 
 * given therapeutic usage, but rather to an available unit of dosage as 
 * may be {@link ProductComponent contained within} a {@link Product product}.
 * </p>
 * 
 * <p>
 * Logically, this allows for composite dosages to be built. As an example: a
 * dose of some medication may be three pills, each containing 25 milligrams 
 * of a given ingredient. This may be de-composed into a dose unit specifying 
 * a quantity of three, and a {@link #unit() unit} (which is itself a 
 * {@link Dose dose unit}) specifying a {@link UnitOfMeasure unit of 
 * measurement} corresponding to "milligrams" and a {@link #quantity() 
 * quantity} of three.
 * </p>
 * 
 * <p>
 * Note that this does not specify an active ingredient.  As the dosage unit
 * model is meant strictly as a means of quantification, composite products
 * (i.e. those composed of various quantities of multiple ingredients) are
 * modeled separately.
 * </p>
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
@JsonTypeName("dose")
@ApiModel(value = "dose", 
			description = "A unit of dosage, comprised of two parts: a unit "
					+ "of measurement, and a finite, non-negative quantity "
					+ "implied to be of that unit.  A dose does not "
					+ "necessarily refer to a specific quantity for a given "
					+ "therapeutic dosage, but rather to an available unit "
					+ "of dosage.")		
public final class Dose
extends UnitOfMeasure
{
	/**
	 * <p>
	 * A singleton instance corresponding to an undefined dosage unit.
	 * </p>
	 * 
	 * <p>
	 * You should avoid using this unless absolutely necessary (i.e. you are 
	 * working with a source of information which offers no concept of units 
	 * and/or does not provide dosing information.)
	 * </p>
	 */
	public static final Dose		UNDEFINED = 
			new Dose("UNDEFINED", UnitOfMeasure.UNDEFINED, 0, false);
	
	private final UnitOfMeasure		unit;
	private final Number			quantity;
	
	private Dose(final String name, final UnitOfMeasure unit, 
					final Number quantity, final boolean fractional)
	{
		super(name, fractional);
		
		this.unit = unit;
		this.quantity = quantity;
	}
	
	/**
	 * Create a new dose consisting of a given integer quantity of a given unit, 
	 * with a given name, optionally allowing for 
	 * {@link #fractional() fractional} usage.
	 * 
	 * @param name name of the dose
	 * @param unit unit of dosage
	 * @param quantity quantity of the given unit which makes up a dose, must
	 * be &gt; 0
	 * @param fractional <code>true</code> if the dosage unit itself may be
	 * fractional, <code>false</code> otherwise (note: non-fractional doses
	 * may still use units which may themselves be fractional)
	 */
	public static final Dose
	create(String name, UnitOfMeasure unit, int quantity, 
					boolean fractional)
	{
		return Dose.create(name, unit, BigInteger.valueOf(quantity), fractional);
	}
	
	/**
	 * Create a new dose consisting of a given integer quantity of a given unit, 
	 * with a given name, optionally allowing for 
	 * {@link #fractional() fractional} usage.
	 * 
	 * @param name name of the dose
	 * @param unit unit of dosage
	 * @param quantity quantity of the given unit which makes up a dose, must
	 * be &gt; 0
	 * @param fractional <code>true</code> if the dosage unit itself may be
	 * fractional, <code>false</code> otherwise (note: non-fractional doses
	 * may still use units which may themselves be fractional)
	 */
	public static final Dose
	create(String name, UnitOfMeasure unit, BigInteger quantity, 
					boolean fractional)
	{
		final Dose d = new Dose(name, unit, quantity, fractional);

		if (d.equals(UNDEFINED))
		{
			return UNDEFINED;
		}
		
		if (quantity.compareTo(BigInteger.ZERO) < 1)
		{
			throw new IllegalArgumentException(
					"quantity must be greater than zero");
		}
				
		return d;
	}
	
	/**
	 * Create a new dose consisting of a given decimal quantity of a given unit, 
	 * with a given name, optionally allowing for 
	 * {@link #fractional() fractional} usage.  Note that this method will 
	 * fail if the give unit is non-fractional, but a quantity which is not 
	 * a whole number is specified.  Callers which know that their quantities
	 * are whole numbers should use 
	 * {@link #create(String, UnitOfMeasure, BigInteger, boolean)} instead.
	 * 
	 * @param name name of the dose
	 * @param unit unit of dosage
	 * @param quantity quantity of the given unit which makes up a dose, must 
	 * be &gt; 0
	 * @param fractional <code>true</code> if the dosage unit itself may be
	 * fractional, <code>false</code> otherwise (note: non-fractional doses
	 * may still use units which may themselves be fractional)
	 */
	public static final Dose
	create(String name, UnitOfMeasure unit, BigDecimal quantity, 
					boolean fractional)
	{
		if (! unit.fractional() && ! isWhole(quantity))
		{
			throw new IllegalArgumentException(String.format(
					"Unit '%s' is not fractional, but decimal quantity "
					+ "'%s' specified", unit.name(), quantity.toPlainString()));
		}
		return new Dose(name, unit, quantity, fractional);
	}

	/**
	 * The unit of measurement in a dose.  May itself be a dosage unit.
	 * 
	 * @return unit of measurement of a dose, not <code>null</code>
	 */
	@NotNull
	@JsonGetter("unit")
	@ApiModelProperty(value = "Unit of measurement in a dose.  May itself "
							+ "be a dosage unit.")
	public UnitOfMeasure
	unit()
	{
		return this.unit;
	}
	
	/* overridden here solely for documentation. */
	@ApiModelProperty(value = "Name of the dosage unit.",
						example = "tablet")
	@Override
	public String
	name()
	{
		// TODO Auto-generated method stub
		return super.name();
	}

	/**
	 * Quantity of the {@link #unit() unit} required to make up a complete 
	 * dose.
	 * 
	 * @return quantity, not <code>null</code>, must be positive
	 */
	@NotNull
	@Min(value = 0)
	@JsonIgnore
	public Number 
	quantity()
	{
		return this.quantity;
	}
	
	/**
	 * Quantity, formatted as a string.  Exists solely for use in serialization
	 * to/from JSON.
	 * 
	 * @return quantity string, not <code>null</code>
	 */
	@NotNull
	@JsonGetter("quantity")
	@ApiModelProperty(value = "Quantity of the unit required to make up a "
							+ "complete dose. Represented as a string to ensure "
							+ "precision when handling arbitrary-precision "
							+ "decimal values.",
						example = "200")
	public String
	quantityString()
	{
		final Number q = this.quantity();
		
		if (q instanceof BigDecimal)
		{
			return ((BigDecimal) q).toPlainString();
		}
		
		return q.toString();
	}
	
	/**
	 * Determine if the given decimal represents a whole number.
	 * 
	 * @param d decimal number to check
	 * @return <code>true</code> if the decimal represents a whole number,
	 * <code>false</code> otherwise
	 */
	private static final boolean
	isWhole(BigDecimal d)
	{
		  return d.signum() == 0 
				  || d.scale() <= 0 
				  || d.stripTrailingZeros().scale() <= 0;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((quantity == null) ? 0 : quantity.hashCode());
		result = prime * result + ((unit == null) ? 0 : unit.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dose other = (Dose) obj;
		if (quantity == null)
		{
			if (other.quantity != null)
				return false;
		}
		else if (!quantity.equals(other.quantity))
			return false;
		if (unit == null)
		{
			if (other.unit != null)
				return false;
		}
		else if (!unit.equals(other.unit))
			return false;
		return true;
	}
	
	@Override
	public String 
	toString()
	{
		return String.format("%s %s", this.quantity().toString(), this.unit().toString());
	}
	
	
}
