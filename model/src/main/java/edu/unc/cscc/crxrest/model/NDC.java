/*-
 * ========================LICENSE_START=================================
 * model
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.crxrest.model;

import java.util.Arrays;
import java.util.regex.Pattern;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonValue;

import edu.unc.cscc.crxrest.model.barcode.UPCA;

/**
 * <p>
 * An NDC, or National Drug Code.  An NDC may be in one of four formats, 
 * consisting of either 10 or 11 digits: 4-4-2, 5-3-2, 5-4-1, or 5-4-2.
 * </p>
 * 
 * <p>
 * This implementation deliberately allows for the package code to be omitted,
 * so as to represent drugs/products for which the packaging is immaterial
 * or unknown.  In these cases, the <i>partial</i> NDC will specify an empty
 * array for its package code.
 * </p>
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
@JsonTypeName("ndc")
public final class NDC
{

	/**
	 * Regular expression for validating full NDC codes in any of the known
	 * formats.
	 */
	public static final String		REGEX = 
			"(\\d{4}\\-\\d{4}\\-\\d{2})"
			+ "|(\\d{5}\\-((\\d{4}\\-\\d{1,2})"
						+ "|(\\d{3}\\-\\d{2})))";
	
	private static final String		PARTIAL_REGEX = 
			"(\\d{4}\\-\\d{4})"
			+ "|(\\d{5}\\-\\d{3,4})";
		
	private final int[]				labelerCode;
	private final int[]				productCode;
	private final int[]				packageCode;
	
	/**
	 * Construct a new partial NDC consisting of the given labeler and product
	 * codes.  The lengths of the given codes will be checked to ensure that 
	 * the given NDC conforms to one of the standard formats (excl. package 
	 * code).
	 * 
	 * @param labelerCode labeler code, 4 or 5 digits
	 * @param productCode product code, 3 or 4 digits
	 */
	public NDC(int[] labelerCode, int[] productCode)
	{
		this(labelerCode, productCode, null);
	}

	/**
	 * Construct a new NDC consisting of the given labeler, product, and 
	 * (optionally) package codes.  The lengths of the given codes will be 
	 * checked to ensure that the resulting NDC will be in one of the standard
	 * formats (4-4-2, 5-3-2, 5-4-1, or 5-4-2).
	 * 
	 * @param labelerCode labeler code, 4 or 5 digits
	 * @param productCode product code, 3 or 4 digits
	 * @param packCode package code, 1 or 2 digits if present, may be 
	 * <code>null</code> or empty for partial NDCs
	 */
	public NDC(int[] labelerCode, int[] productCode, int[] packCode)
	{
		/* ensure that package code is non-null */
		
		final int[] packageCode = packCode == null ? new int[0] : packCode;
		
		/* sanity checking for the code components */
		
		if (! inRange(labelerCode))
		{
			throw new IllegalArgumentException("Labeler code must consist of "
					+ "digits in 0-9 range (inclusive)");
		}
		
		if (! inRange(productCode))
		{
			throw new IllegalArgumentException("Product code must consist of "
					+ "digits in 0-9 range (inclusive)");
		}
		
		if (packageCode != null && ! inRange(packageCode))
		{
			throw new IllegalArgumentException("Package code must consist of "
					+ "digits in 0-9 range (inclusive) OR must be empty if "
					+ "no package code is known/specified");
		}
		
		/* check that we're in one of the recognized formats */
		
		if (labelerCode.length == 5)
		{
			if (productCode.length < 3)
			{
				throw new IllegalArgumentException("5 digit labeler code "
						+ "specified, but product code was < 3 digits");
			}
			else if (productCode.length == 3)
			{
				if (packageCode.length > 0 && packageCode.length != 2)
				{
					throw new IllegalArgumentException("5 digit labeler and "
							+ "3 digit product code, but package code was "
							+ "<> 0/2 digits");
				}
			}
			else if (productCode.length == 4)
			{
				if (packageCode.length > 2)
				{
					throw new IllegalArgumentException("5 digit labeler and "
							+ "4 digit product code, but package code was "
							+ "> 2 digits");
				}
			}
			else
			{
				throw new IllegalArgumentException("5 digit labeler, but "
						+ "product code was <> 3/4 digits");
			}
		}
		else if (labelerCode.length == 4)
		{
			if (productCode.length != 4)
			{
				throw new IllegalArgumentException("4 digit labeler code "
						+ "specified, but product code was <> 4 digits");
			}
			
			if (packageCode.length > 0 && packageCode.length != 2)
			{
				throw new IllegalArgumentException("4 digit labeler and "
						+ "product code, but package code was <> 0/2 digits");
			}
		}
		
		this.labelerCode = labelerCode;
		this.productCode = productCode;
		this.packageCode = packageCode;
	}
	
	/**
	 * Get the labeler code component of the NDC.  The resulting code will be four or 
	 * five digits long, depending on NDC format.
	 * 
	 * @return labeler code, not <code>null</code>
	 */
	public int[]
	labelerCode()
	{
		return Arrays.copyOf(this.labelerCode, this.labelerCode.length);
	}
	
	/**
	 * Get the product code component of the NDC.  The resulting code will be
	 * three or four digits long, depending on NDC format.
	 * 
	 * @return product code, not <code>null</code>
	 */
	public int[]
	productCode()
	{
		return Arrays.copyOf(this.productCode, this.productCode.length);
	}
	
	/**
	 * Get the package code component of the NDC.  If the NDC is complete, 
	 * the resulting code will be one or two digits long, depending on NDC 
	 * format.  In the case of a partial NDC, the resulting code will be empty.
	 * 
	 * @return package code, not <code>null</code>, may be empty
	 */
	public int[]
	packageCode()
	{
		if (this.packageCode.length > 0)
		{
			return Arrays.copyOf(this.packageCode, this.packageCode.length);
		}
		
		return new int[0];
	}
	
	/**
	 * Convenience method to determine whether the NDC is complete (i.e. 
	 * contains all three components.)
	 * 
	 * @return <code>true</code> if the NDC is complete, <code>false</code>
	 * otherwise
	 */
	public boolean
	complete()
	{
		return this.packageCode.length > 0;
	}
	
	/**
	 * Alias for {@link #toString()}. Produces a normalized/formatted
	 * representation of the NDC.
	 * 
	 * @return string value of the NDC
	 */
	@JsonValue
	public String
	value()
	{
		return this.toString();
	}

	/**
	 * Produce a normalized (5-4-2) representation of this NDC.  If the NDC
	 * is not complete, the labeler and product codes will be normalized.
	 * 
	 * @return normalized NDC in 5-4-2 format (if possible)
	 */
	public NDC
	normalize()
	{
		
		if (this.normalized())
		{
			return this;
		}
		
		if (this.labelerCode.length == 4)
		{
			return new NDC(ArrayUtils.add(this.labelerCode, 0, 0),
							this.productCode, this.packageCode);
		}
		
		if (this.productCode.length == 3)
		{
			return new NDC(this.labelerCode, 
							ArrayUtils.add(this.productCode, 0, 0), 
							this.packageCode);
		}
		
		if (this.packageCode != null && this.packageCode.length == 1)
		{
			return new NDC(this.labelerCode, 
					this.productCode,
					ArrayUtils.add(this.packageCode, 0, 0));
		}
		
		throw new IllegalArgumentException(
				"Cannot normalize malformed NDC.  This is a bug; please "
				+ "report it.");
	}
	
	/**
	 * Whether the NDC is in normalized (11-digit, 5-4-2) form.
	 * 
	 * @return <code>true</code> if the NDC is in 5-4-2 form, 
	 * <code>false</code> otherwise
	 */
	public boolean
	normalized()
	{
		return this.labelerCode.length == 5
				&& this.productCode.length == 4
				&& (this.packageCode.length == 0 || this.packageCode.length == 2);
	}
	
	@Override
	public String
	toString()
	{
		final StringBuilder b = new StringBuilder();
		
		for (final int a : this.labelerCode)
		{
			b.append(a);
		}
		
		b.append('-');
		
		for (final int a : this.productCode)
		{
			b.append(a);
		}
		
		if (this.packageCode.length > 0)
		{
			b.append('-');
			
			for (final int a : this.packageCode)
			{
				b.append(a);
			}
		}
		return b.toString();
	}
	
	/**
	 * Parse a string representation of an NDC.
	 * 
	 * @param str string representation of NDC, not <code>null</code>
	 * @return NDC corresponding to the given string representation
	 * @throws IllegalArgumentException thrown if the input was malformed
	 */
	@JsonCreator
	public static final NDC
	parse(@NotNull final String str)
	throws IllegalArgumentException
	{
		if (str == null)
		{
			throw new IllegalArgumentException("input was null");
		}
			
		final String s = StringUtils.removeAll(str, "\\s");
		
		/* handle numeric-only 11-digit NDCs */
		if (StringUtils.isNumericSpace(s))
		{
			if (s.length() == 11)
			{
				final int[] code = new int[11];
				final char[] chars = s.toCharArray();
				
				for (int i = 0; i < chars.length; i++)
				{
					code[i] = chars[i] - '0';
				}
				
				return create(code);
			}
		}
		
		if (! Pattern.matches(REGEX, s))
		{
			/* check if we can parse it as a partial NDC */
			if (! Pattern.matches(PARTIAL_REGEX, s))
			{
				throw new IllegalArgumentException(
						"NPC must consist of numeric values, optionally in "
						+ "dash-delimited form (for 10-digit or partial NDCs)");
			}
			
			/* partial NDC */
		}
		
		/* now we can split on dashes, convert to digits, and we're done */
		final int[][] digits = new int[3][]; 
		
		int g = 0;
		for (final String seg : s.split("\\Q-\\E"))
		{
			final char[] chars = seg.toCharArray();
			digits[g] = new int[chars.length];
			
			for (int i = 0; i < chars.length; i++)
			{
				digits[g][i] = chars[i] - '0';
			}
			g++;
		}
		
		return new NDC(digits[0], digits[1], digits[2]);
	}
	
	/**
	 * Create an NDC from its normalized 11 digit representation.  
	 * 
	 * @param digits digits comprising the normalized NDC
	 * @return NDC in {@link Grouping#FIVE_FOUR_TWO 5-4-2} form containing the
	 * given digits
	 */
	public static final NDC
	create(int[] digits)
	{
		if (digits.length != 11)
		{
			throw new IllegalArgumentException(
					"Non-segmented NDCs must be 11 digits long");
		}
		
		/* split into 5-4-2 */
		
		final int[] label = Arrays.copyOfRange(digits, 0, 5);
		final int[] prod = Arrays.copyOfRange(digits, 5, 9);
		final int[] pack = Arrays.copyOfRange(digits, 9, 11);
		
		return new NDC(label, prod, pack);
	}
	
	/**
	 * Extract the possible NDCs (if any) from a given UPC-A barcode.
	 * 
	 * @param upc UPC, not <code>null</code>
	 * @return NDCs, not <code>null</code>, may have length of 0
	 */
	public static final NDC[]
	possibleNDCs(UPCA upc)
	{
		final int[] upn = upc.toUPN();
		
		if (upn == null || upn.length != 10)
		{
			return new NDC[0];
		}
	
		return new NDC[] {
			NDC.create(ArrayUtils.add(upn, 0, 0)),
			NDC.create(ArrayUtils.add(upn, 5, 0)),
			NDC.create(ArrayUtils.add(upn, 9, 0))
		};
		
	}
	
	private static final boolean
	inRange(int[] digits)
	{
		for (final int a : digits)
		{
			if (a < 0 || a > 9)
			{
				return false;
			}
		}
		
		return true;
	}
	
}
