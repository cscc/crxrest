/*-
 * ========================LICENSE_START=================================
 * model
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.crxrest.model.barcode;

import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;

/**
 * A barcode in the UPC-A form (sometimes referred to as "UPC"). Contains 12
 * digits: 11 payload, and 1 check digit.
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
public final class UPCA
extends AbstractBarcode<int[]>
{
	
	/**
	 * Construct a new UPC, consisting of the given data.
	 * 
	 * @param data contents of the UPC
	 */
	UPCA(int[] data)
	{
		super(Arrays.copyOf(data, data.length), CodedDataType.NUMERIC);
	}

	/**
	 * Attempt to parse the given input string into an 11/12 digit UPC-A.
	 * 
	 * 
	 * @param barcode string containing string representation of the UPC
	 * @return UPC containing the specified code as its payload
	 * 
	 * @throws MalformedBarcodeException thrown if parsing failed for any 
	 * reason
	 */
	public static final UPCA
	parse(String barcode)
	throws MalformedBarcodeException
	{
		if (! StringUtils.isNumericSpace(barcode))
		{
			throw new MalformedBarcodeException(
					"UPC-A must consist of numeric values only");
		}
		
		final String str = StringUtils.removeAll(barcode, "\\s");
		
		final char[] chars = str.toCharArray();
		
		if (chars.length < 11 || chars.length > 13)
		{
			throw new MalformedBarcodeException(
					"UPC-A must consist of 11 or 12 digits");
		}
		
		final int[] digits = new int[chars.length];
		
		int sum = 0;
		
		for (int i = 0; i < 11; i++)
		{
			digits[i] = chars[i] - '0';
			sum += (i % 2 == 0 
					? digits[i] * 3 
					: digits[i]);
		}
		if (chars.length > 11)
		{
			digits[11] = chars[11] - '0';
		}
		
		int check = sum  % 10;
		
		if (check != 0)
		{
			check = 10 - check;
		}
		
		if (digits.length > 11)
		{
			if (digits[11] != check)
			{
				throw new MalformedBarcodeException(
						String.format("Bad check digit; expected check digit "
										+ "%d got %d", check, digits[11]));
			}
			
			return new UPCA(digits);
		}
		
		/* append check digit */
		int[] newDigits = Arrays.copyOf(digits, 12);
		newDigits[11] = check;
		return new UPCA(newDigits);
	}

	/**
	 * Get the UPN contained within this barcode if one exists (i.e. if the 
	 * number system digit indicates that the UPC contains an NDC.)
	 * 
	 * @return representation of the UPN, or <code>null</code> if no 
	 * UPN contained
	 */
	public int[] 
	toUPN()
	{
		
		if (this.data()[0] != 3)
		{
			return null;
		}
		
		return Arrays.copyOfRange(this.data(), 1, 11);
	}

	@Override
	public final String 
	standardIdentifier()
	{
		return "UPC-A";
	}
	
	@Override
	public String 
	toString()
	{
		String str = "";
		for (int d : this.data())
		{
			str += String.valueOf(d);
		}
		return str;
	}
	
	
	
}
