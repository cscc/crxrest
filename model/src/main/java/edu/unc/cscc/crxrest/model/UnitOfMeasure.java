/*-
 * ========================LICENSE_START=================================
 * model
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.crxrest.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * A unit of measurement.  Examples: a "gram", a "25 milligram tablet".  This
 * is deliberately left flexible so as to accommodate dictionary sources which
 * provide alternative measurements of drugs (ex: number of tablets in a dose
 * is known, but quantity of active ingredient is not specified.)
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
@JsonTypeName("unit")
@ApiModel(value = "unit", 
			description = "A unit of measurement.  This is deliberately left "
						+ "flexible so as to accommodate dictionary sources "
						+ "which provide alternative measurements of drugs "
						+ "(ex: number of tablets in a dose is known, but "
						+ "quantity of active ingredient is not specified.)")
public class UnitOfMeasure
{
	private static final String			UNDEFINED_NAME = "UNDEFINED";

	/**
	 * A singleton instance corresponding to an undefined unit of measure.
	 * You should avoid using this unless absolutely necessary (i.e. you are 
	 * working with a source of information which offers no concept of units.)
	 */
	public static final UnitOfMeasure	UNDEFINED = 
			new UnitOfMeasure(UnitOfMeasure.UNDEFINED_NAME, false);
	
	
	private final boolean				fractional;
	private final String				name;
	
	/**
	 * Create a new unit with a given name.
	 * 
	 * @param name name of unit
	 * @param fractional <code>true</code> if the unit can be 
	 * {@link #fractional() fractional}, <code>false</code> otherwise
	 */
	@JsonCreator
	public UnitOfMeasure(@JsonProperty("name") String name, 
						@JsonProperty("fractional") boolean fractional)
	{
		this.name = name;
		this.fractional = fractional;
	}
	
	/**
	 * Whether fractional units (represented as decimal values) are allowed 
	 * for this unit.
	 * 
	 * @return <code>true</code> if fractional units are allowed, 
	 * <code>false</code> otherwise
	 */
	@JsonGetter("fractional")
	@ApiModelProperty("Whether fractional units (represented as decimal "
						+ "values) are allowed for this unit.")
	public boolean 
	fractional()
	{
		return this.fractional;
	}
	
	/**
	 * Name of the unit.
	 * 
	 * @return name of the unit, not <code>null</code>
	 */
	@NotNull
	@Size(min = 1)
	@JsonGetter("name")
	@ApiModelProperty(value = "Name of the unit.  Free-form human-readable string.",
						example = "mg")
	public String
	name()
	{
		return this.name;
	}
	
	@Override
	public String
	toString()
	{
		return this.name();
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + (fractional ? 1231 : 1237);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UnitOfMeasure other = (UnitOfMeasure) obj;
		if (fractional != other.fractional)
			return false;
		if (name == null)
		{
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
}
