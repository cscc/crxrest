/*-
 * ========================LICENSE_START=================================
 * model
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.crxrest.model.barcode;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonTypeName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * A barcode is a graphical representation of a small amount of data.  
 * Implementations may allow for the representation of numeric, textual, or 
 * binary data.  This interface implements functionality common to all 
 * supported barcoding schemes.
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 * @param <T> type of data represented by the barcode
 */
@JsonTypeName("barcode")
@ApiModel(value = "barcode", 
			description = "A barcode is a graphical representation of a "
						+ "small amount of data. Implementations may allow "
						+ "for the representation of numeric, textual, or "
						+ "binary data.")
public interface Barcode<T>
{
	/**
	 * The supported data types of barcode data.
	 */
	public static enum CodedDataType
	{
		/**
		 * Numeric data.  A sequence of digits.
		 */
		NUMERIC {
			@SuppressWarnings("unchecked")
			@Override
			public Class<int[]> cls()
			{
				return int[].class;
			}
		},
		/**
		 * Alphanumeric data.  Represented as a character sequence/string.
		 */
		TEXT {
			@SuppressWarnings("unchecked")
			@Override
			public Class<String> cls()
			{
				return String.class;
			}
		},
		/**
		 * Binary data.  Represented as a byte sequence.
		 */
		BINARY {
			@SuppressWarnings("unchecked")
			@Override
			public Class<byte[]> cls()
			{
				return byte[].class;
			}
		};
		
		/**
		 * Get the appropriate class for representation of coded data of this
		 * type.
		 * 
		 * @return class, not <code>null</code>
		 */
		public abstract <C> Class<C> cls();
	}
	
	/**
	 * The data type of the barcode's payload.
	 * 
	 * @return data type, not <code>null</code>
	 */
	@NotNull
	@JsonGetter("data-type")
	@ApiModelProperty(value = "The data type of the barcode's payload.",
						example = "NUMERIC")
	public CodedDataType dataType();
	
	/**
	 * Get the data conveyed by this barcode.
	 * 
	 * @return data contained in barcode, may be <code>null</code>
	 */
	@JsonGetter("data")
	@ApiModelProperty(value = "The data conveyed by the barcode.",
						allowEmptyValue = true,
						example = "681131700207")
	public T data();
	
	/**
	 * String identifier of the barcoding standard to which this barcode 
	 * complies.  In cases where a barcode complies with multiple standards,
	 * the barcode should return the generally-accepted identifier of the preferred 
	 * representation.
	 * 
	 * @return barcode standard identifier. not <code>null</code>
	 */
	@NotNull
	@JsonGetter("standard-identifier")
	@ApiModelProperty(value = "String identifier of the barcoding standard to "
							+ "which this barcode complies.  In cases where "
							+ "a barcode complies with multiple standards, "
							+ "the barcode should return the "
							+ "generally-accepted identifier of the "
							+ "preferred representation.",
						example = "UPC-A")
	public String standardIdentifier();
		
	/**
	 * Exception for use when handling a malformed or logically-invalid 
	 * barcode.
	 *
	 */
	public static class MalformedBarcodeException
	extends Exception
	{

		private static final long serialVersionUID = 1L;
		
		public MalformedBarcodeException(String msg)
		{
			super(msg);
		}
		
		public MalformedBarcodeException(String msg, Throwable cause)
		{
			super(msg, cause);
		}
		
	}
}
