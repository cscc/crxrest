/*-
 * ========================LICENSE_START=================================
 * model
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.crxrest;

import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

import edu.unc.cscc.crxrest.model.ATCCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * Represents a dictionary within CRxREST.  A dictionary may be used for 
 * coding medications as well as for searching for medications based on an
 * input string; a "dictionary" represents the dataset itself, and is used 
 * in conjunction with a {@link DictionaryService supporting service}.
 * </p>
 * 
 * <p>As this interface is intended for client discovery and is thus 
 * serialized, care should be taken to ensure that implementations are 
 * a) thread-safe and cacheable b) stateless.
 * </p> 
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
@JsonTypeName("dictionary")
public interface Dictionary
{
	
	/**
	 * A unique identifier of the dictionary in question.
	 * 
	 * @return unique identifier, not <code>null</code>, must be &gte; 1 
	 * character
	 */
	@NotNull
	@Size(min = 1)
	@JsonGetter("identifier")
	@ApiModelProperty(value = "unique identifier of the dictionary",
						example = "NDC-DB")
	public String identifier();
	
	/**
	 * A path and/or description of the source material for the dictionary. 
	 * An example might include a filename, revision number, and date.  This
	 * is not used by CRxREST directly, but may be useful when determining the
	 * provenance of a result.
	 * 
	 * <p>
	 * As a last resort, this may be an empty string, however implementations
	 * are strongly encouraged to provide a meaningful source description
	 * here.
	 * </p>
	 * 
	 * @return dictionary source, not <code>null</code>
	 */
	@NotNull
	@JsonGetter("source")
	@ApiModelProperty(value = "A path or description of the source material for "
							+ "the dictionary.  This is not used by CRxREST "
							+ "directly, but may be useful when determining "
							+ "the provenance of a result.",
						example = "NDC-2017-07-17;CSV")
	public String source();
	
	/**
	 * Get the capabilities supported by this dictionary.
	 * 
	 * @return set of supported capabilities, not <code>null</code>
	 */
	@NotNull
	@JsonGetter("capabilities")
	@ApiModelProperty("capabilities support by the dictionary")
	public Set<DictionaryCapabilities> capabilities();
	
	/**
	 * Convenience method for determining if a dictionary supports a given 
	 * capability.  Equivalent to checking whether the dictionary's 
	 * {@link #capabilities() set of capabilities} contains the given 
	 * capability.
	 * 
	 * @param capability capability for which to check support
	 * @return <code>true</code> if the dictionary supports the given 
	 * capability, <code>false</code> otherwise
	 */
	public default boolean 
	supports(DictionaryCapabilities capability)
	{
		return this.capabilities().contains(capability);
	}
	
	/**
	 * Get the license of the specified dictionary.
	 * 
	 * @return license, not <code>null</code>
	 */
	@NotNull
	@JsonGetter("license")
	@ApiModelProperty("the license under which the dictionary is available")
	public License license();
	
	public static enum DictionaryCapabilities
	{
		/**
		 * Dictionary contains barcode information.
		 */
		BARCODES,
		/**
		 * Dictionary contains trade names of drugs (independent of the 
		 * names of products containing said drugs.)
		 */
		TRADE_NAMES,
		/**
		 * Dictionary provides first-class support for compound drugs (i.e.
		 * drugs composed of multiple ingredients).
		 */
		COMPOUND_DRUGS,
		/**
		 * Dictionary contains chemical formulas of drugs.
		 */
		CHEMICAL_FORMULAS,
		/**
		 * National Drug Code (FDA)
		 */
		NDC,
		/**
		 * {@link ATCCode ATC codes}
		 */
		ATC,
		SIMPLE;
	}
	
	/**
	 * Represents a license under which a specified dictionary may be used.
	 *
	 */
	@JsonTypeName("license")
	@ApiModel(value = "license", 
				description = "A license or set of terms under which a "
							+ "specified dictionary may be used.")
	public static final class License
	{
		private final String	name;
		private final String	text;
		
		@JsonCreator
		public License(@JsonProperty("name") final String name, 
						@JsonProperty("license-text") final String text)
		{
			this.name = name;
			this.text = text;
		}

		/**
		 * Get the name of the license.  This is generally a unique identifier
		 * such as "GPLv2" or "Creative Commons NonCommercial 4.0".  It should
		 * be sufficient to identify a published license without having to 
		 * consult the license text directly.
		 * 
		 * @return license name, not <code>null</code>
		 */
		@NotNull
		@Size(min = 1)
		@JsonGetter("name")
		@ApiModelProperty(value = 
							"The name of the license.  Should be sufficient to "
							+ "identify a published license without having to consult "
							+ "the license text directly.",
						example = "Public Domain (US)")
		public final String 
		name()
		{
			return this.name;
		}

		/**
		 * Text of the dictionary license.
		 * 
		 * @return license text, not <code>null</code>
		 */
		@NotNull
		@Size(min = 1)
		@JsonGetter("license-text")
		@ApiModelProperty(value = "The text of the license.",
							example = "The contents of this dictionary are "
									+ "placed in the public domain.")
		public final String 
		text()
		{
			return this.text;
		}

		@Override
		public int hashCode()
		{
			final int prime = 31;
			int result = 1;
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			result = prime * result + ((text == null) ? 0 : text.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj)
		{
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			License other = (License) obj;
			if (name == null)
			{
				if (other.name != null)
					return false;
			}
			else if (!name.equals(other.name))
				return false;
			if (text == null)
			{
				if (other.text != null)
					return false;
			}
			else if (!text.equals(other.text))
				return false;
			return true;
		}
		
		/**
		 * Singleton instance representing an unknown license.
		 */
		public static final License UNKNOWN = 
				new License("UNKNOWN", 
						"License information unknown. PROCEED WITH CAUTION!");
		
		
	}
	
	

}
