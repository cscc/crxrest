/*-
 * ========================LICENSE_START=================================
 * model
 * %%
 * Copyright (C) 2017 - 2024 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.crxrest.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@JsonTypeName("simple")
@ApiModel(value = "simple",
			description = "A simple name for a result.")
public class Simple {
	
private final String name;
	
	/**
	 * Create a simple reference from a string.
	 * 
	 * @param name name, not <code>null</code>, &ge; 1 characters
	 */
	@JsonCreator
	public Simple(@JsonProperty("name") String name)
	{
		this.name = name;
	}
	
	/**
	 * The simple name.
	 * 
	 * @return name, not <code>null</code>
	 */
	@NotNull
	@Size(min = 1)
	@JsonGetter("name")
	@ApiModelProperty(value = "The simple name.",
					example = "Blueberries")
	public String
	name()
	{
		return this.name;
	}
	
	/**
	 * Get the display name that should be used when presenting this result
	 * to a user.  By default, this delegates to {@link #name()}.
	 * 
	 * @return display name, not <code>null</code>
	 */
	@JsonGetter("display-name")
	@ApiModelProperty(value = "A human-readable name for use when presenting "
							+ "the result to a user, usually an alias for "
							+ "the 'name' property.",
					  example = "Computer Programmer")
	public String displayName() {
		return this.name();
	}
	
	@Override
	public String
	toString()
	{
		return this.name();
	}
	
	@Override
	public int 
	hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean 
	equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Simple other = (Simple) obj;
		if (name == null)
		{
			if (other.name != null)
				return false;
		}
		else if (!name.equals(other.name))
			return false;
		return true;
	}

}
