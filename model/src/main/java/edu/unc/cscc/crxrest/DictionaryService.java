/*-
 * ========================LICENSE_START=================================
 * model
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.crxrest;

import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import edu.unc.cscc.crxrest.model.Drug;
import edu.unc.cscc.crxrest.model.NDC;
import edu.unc.cscc.crxrest.model.Simple;
import edu.unc.cscc.crxrest.model.TradeName;
import edu.unc.cscc.crxrest.model.barcode.Barcode;
import edu.unc.cscc.crxrest.model.product.Product;

public interface DictionaryService
{
	/**
	 * The maximum number of results that the service may produce in response
	 * to a given query.  Implementations are encouraged to choose this limit
	 * based on search performance.
	 * 
	 * @return maximum number of results that can be practically produced in
	 * response to a query, &gt; 0
	 */
	@Min(1)
	public int resultLimit();
	
	/**
	 * The dictionary represented by the service.
	 * 
	 * @return dictionary, not <code>null</code>
	 */
	@NotNull
	public Dictionary dictionary();
	
	/**
	 * Find the trade names matching the given query.  The logic used to 
	 * determine suitable search results is left up to the service 
	 * implementation, but should at least include those tradenames for which
	 * the input query is a substring.
	 * 
	 * <p>
	 * Implementations should attempt to sort results by relevance.
	 * </p>
	 * 
	 * @param query query for which to find trade names, not <code>null</code>
	 * and not empty
	 * @param limit maximum number of results to produce, negative value if 
	 * the service's {@link #resultLimit() maximum results limit} should be 
	 * used
	 * 
	 * @return matching trade names, not <code>null</code>, empty if no matches
	 */
	@NotNull
	public List<TradeName> findTradeNames(String query, int limit);
	
	/**
	 * Find drugs whose names and/or properties match a given query.  The
	 * exact logic used to determine "match" is left up to the service 
	 * implementation, but should at least include those drugs for which
	 * the input query is a substring of the name. 
	 * 
	 * <p>
	 * Implementations should attempt to sort results by relevance.
	 * </p>
	 * 
	 * @param query input for which to find drugs, not <code>null</code>
	 * @param limit maximum number of results to produce, negative value if 
	 * the service's {@link #resultLimit() maximum results limit} should be 
	 * used
	 * 
	 * @return matching drugs, not <code>null</code>, empty if no matches
	 */
	@NotNull
	public List<Drug> findDrugs(String query, int limit);
	
	/**
	 * Find products whose names and/or components match a given query.  The
	 * exact logic used to determine "match" is left up to the service 
	 * implementation, but should at least include those products for which
	 * the input query is a substring.  Implementations are encouraged (but
	 * not required) to consider product components when determining matches.
	 * 
	 * <p>
	 * Implementations should attempt to sort results by relevance.
	 * </p>
	 * 
	 * @param query input for which to find products, not <code>null</code>
	 * @param limit maximum number of results to produce, negative value if 
	 * the service's {@link #resultLimit() maximum results limit} should be 
	 * used
	 * 
	 * @return matching products, not <code>null</code>, empty if no matches
	 */
	@NotNull
	public List<Product> findProducts(String query, int limit);
	
	/**
	 * <p>
	 * Attempt to find a product by its NDC.  
	 * </p>
	 * 
	 * <p>
	 * Implementations should attempt to sort results by relevance.
	 * </p>
	 * 
	 * 
	 * @param ndc NDC for which to find product, not <code>null</code>
	 * @param limit maximum number of results to produce, negative value if 
	 * the service's {@link #resultLimit() maximum results limit} should be 
	 * used.
	 * 
	 * @return product (or products in the case of a partial NDC) with the 
	 * given NDC, empty if no such product(s) found, not <code>null</code>
	 */
	@NotNull
	public List<Product> findByNDC(NDC ndc, int limit);
	
	/**
	 * Attempt to find any products with the given barcode.
	 * 
	 * <p>
	 * Implementations should attempt to sort results by relevance.
	 * </p>
	 * 
	 * @param barcode barcode for which to find products, not <code>null</code>
	 * @param limit maximum number of results to produce, negative value if 
	 * the service's {@link #resultLimit() maximum results limit} should be 
	 * used
	 * 
	 * @return
	 */
	public List<Product> findByBarcode(Barcode<?> barcode, int limit);
	
	public default List<Simple> findByName(String query, int limit) {
		throw new UnsupportedOperationException("Not handled by this implementation.");
	}
}
