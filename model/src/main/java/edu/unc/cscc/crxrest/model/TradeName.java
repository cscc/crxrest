/*-
 * ========================LICENSE_START=================================
 * model
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.crxrest.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * A trade name for a drug.  Note that this is <b>NOT</b> the same as a 
 * product name.  A trade name refers to a marketing name given to a 
 * pharmaceutical agent, but not necessarily to the name of a product 
 * containing the ingredient. For example: "Zantac" refers to the drug 
 * "ranitidine", but not to a specific dose (as the name "Zantac 150" would).
 * </p>
 * 
 * <p>
 * Logically, the relationship between this class and {@link Drug} implies
 * a many to many relationship: the same trade name may correspond to one or
 * more drugs, and many drugs may share the same trade name.
 * </p>
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
@JsonTypeName("trade-name")
@ApiModel(value = "trade-name",
			description = "A trade name for a drug.  Note that this is NOT "
						+ "the same as a product name.  A trade name refers "
						+ "to a marketing name given to a pharmaceutical "
						+ "agent, but not necessarily to the name of a "
						+ "product containing the ingredient. For example: "
						+ "\"Zantac\" refers to the drug \"ranitidine\", but "
						+ "not to a specific dose (as the name "
						+ "\"Zantac 150\" would)")
public final class TradeName
{
	private final Drug			drug;
	private final String		name;
	
	/**
	 * Create a trade name from the given text, referring to the specified 
	 * drug.
	 * 
	 * @param name name, not <code>null</code>, &ge; 1 characters
	 * @param drug drug to which the name refers, not <code>null</code>
	 */
	@JsonCreator
	public TradeName(@JsonProperty("name") String name, 
						@JsonProperty("drug") Drug drug)
	{
		this.name = name;
		this.drug = drug;
	}
	
	/**
	 * The trade name.  Trade names may or may not be capitalized depending on
	 * the mark in question.
	 * 
	 * @return trade name, not <code>null</code>
	 */
	@NotNull
	@Size(min = 1)
	@JsonGetter("name")
	@ApiModelProperty(value = "The trade name.  Trade names may or may not be "
							+ "capitalized on the mark in question.",
					example = "Motrin")
	public String
	name()
	{
		return this.name;
	}
	
	@Override
	public String
	toString()
	{
		return this.name();
	}
	
	/**
	 * The drug to which the trade name refers.
	 * 
	 * @return drug to which trade name refers, not <code>null</code>
	 */
	@JsonGetter("drug")
	@NotNull
	@ApiModelProperty("The drug to which the trade name refers.")
	public Drug 
	drug()
	{
		return this.drug;
	}

	@Override
	public int 
	hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((drug == null) ? 0 : drug.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean 
	equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TradeName other = (TradeName) obj;
		if (drug == null)
		{
			if (other.drug != null)
				return false;
		}
		else if (!drug.equals(other.drug))
			return false;
		if (name == null)
		{
			if (other.name != null)
				return false;
		}
		else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	
	
}
