/*-
 * ========================LICENSE_START=================================
 * model
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.crxrest.model.barcode;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Models the EAN-13 barcoding scheme.  EAN-13 barcodes may be converted to 
 * {@link UPCA UPC-A} form depending on their encoding scheme and/or data 
 * content.
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
public final class EAN13
extends AbstractBarcode<int[]>
{

	/**
	 * Construct a new EAN13 barcode containing the given data.  The contents
	 * must be exactly 13 digits (i.e. all elements in the given array 
	 * must be values between 0 and 9 inclusive.
	 * 
	 * @param data contents, not <code>null</code>
	 */
	EAN13(int[] data)
	{
		super(data, CodedDataType.NUMERIC);
	}
	
	/**
	 * Parse an EAN13 barcode from the given string representation.
	 * 
	 * @param barcodeStr string representation of the barcode, not 
	 * <code>null</code>
	 * 
	 * @return barcode with the data parsed from the given string
	 * @throws MalformedBarcodeException thrown if the given string did not
	 * contain valid EAN13 data
	 */
	public static final EAN13
	parse(String barcodeStr)
	throws MalformedBarcodeException
	{
		if (! StringUtils.isNumericSpace(barcodeStr))
		{
			throw new MalformedBarcodeException(
					"EAN13 must consist of numeric values only");
		}
		
		final String str = StringUtils.removeAll(barcodeStr, "\\s");
		
		final char[] chars = str.toCharArray();
		
		if (chars.length < 12 || chars.length > 13)
		{
			throw new MalformedBarcodeException(
					"EAN13 must consist of 12 or 13 digits (depending on "
					+ "check digit inclusion)");
		}
		
		@SuppressWarnings("unused")
		int check = 0;
		
		/* parse + calculate check digit */
		final int[] digits = new int[chars.length];
				
		for (int i = 0; i < 11; i++)
		{
			digits[i] = chars[i] - '0';
		}
		
		throw new IllegalArgumentException("Parsing still TODO");
		
		
		/* TODO implementing EAN13 parsing */
	}

	/**
	 * Get the UPC-A barcode contained within the barcode (if any).  EAN13
	 * barcodes with a first digit of '0' are freely convertible to UPCA
	 * barcodes.
	 * 
	 * @return UPC-A if one exists, or <code>null</code> if the barcode does not
	 * contain a UPC-A code.
	 */
	public UPCA 
	toUPCA()
	{
		final int[] data = this.data();
		
		if (data[0] == 0)
		{
			return new UPCA(ArrayUtils.subarray(data, 1, data.length));
		}
		
		return null;
	}
	
	/**
	 * Convert the given UPCA barcode into an EAN13 barcode (prepending zero.)
	 * 
	 * @param upc UPC to convert, not <code>null</code>
	 * 
	 * @return barcode containing the UPC barcode's data
	 */
	public static final EAN13
	valueOf(UPCA upc)
	{
		return new EAN13(ArrayUtils.add(upc.data(), 0, 0));
	}

	@Override
	public String 
	standardIdentifier()
	{
		return "EAN-13";
	}
	
	
}
