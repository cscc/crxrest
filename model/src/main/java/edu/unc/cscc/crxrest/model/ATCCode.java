/*-
 * ========================LICENSE_START=================================
 * model
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.crxrest.model;

import java.util.Arrays;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

/**
 * Models the ATC coding system.  An ATC code consists of five groupings or 
 * "levels", each comprised of 1-2 digits/characters.  For more information
 * on the meaning of the various components of an ATC code, see the 
 * <a href="https://www.whocc.no/atc/structure_and_principles/">WHOCC documentation</a>.
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
public class ATCCode
{
	public static final String				PATTERN_STR = 
			"^[ABCDGHJLMNPRSV][0-9]{2}[A-Z][A-Z][0-9]{2}$";
	public static final Pattern				PATTERN = 
			Pattern.compile(PATTERN_STR);
	
	
	private final AnatomicalMainGroup		firstLevel;
	private final int[]						secondLevel;
	private final char						thirdLevel;
	private final char						fourthLevel;
	private	final int[]						fifthLevel;
	
	/**
	 * Create a new ATC code consisting of the given parts.
	 * 
	 * @param firstLevel anatomical main group
	 * @param secondLevel therapeutic main group, all elements must be from 0 
	 * to 9 inclusive
	 * @param thirdLevel therapeutic sub-group
	 * @param fourthLevel chemical sub-group
	 * @param fifthLevel chemical substance, all elements must be from 0 to 9
	 * inclusive
	 */
	public ATCCode(final AnatomicalMainGroup firstLevel, 
					final int[] secondLevel, final char thirdLevel, 
					final char fourthLevel, final int[] fifthLevel)
	{
		/* some basic sanity checking */
		
		if (secondLevel[0] < 0 || secondLevel[0] > 9
			|| fifthLevel[0] < 0 || fifthLevel[0] > 9
			|| secondLevel[1] < 0 || secondLevel[1] > 9
			|| fifthLevel[1] < 0 || fifthLevel[1] > 9)
		{
			throw new IllegalArgumentException("Second and fifth levels of "
					+ "ATC code must consist of integers in the 0-9 range");
		}
		
		if (thirdLevel < 65 || thirdLevel > 90
			|| fourthLevel < 65 || fourthLevel > 90)
		{
			throw new IllegalArgumentException("Third and fourth levels of "
					+ "ATC code must consist of chars in the A-Z range");
		}
		
		
		this.firstLevel = firstLevel;
		this.secondLevel = secondLevel;
		this.thirdLevel = thirdLevel;
		this.fourthLevel = fourthLevel;
		this.fifthLevel = fifthLevel;
	}

	/**
	 * Anatomical main group.  First level of the code.
	 * 
	 * @return anatomical main group
	 */
	public final AnatomicalMainGroup
	anatomicalMainGroup()
	{
		return this.firstLevel();
	}
	
	/**
	 * Therapeutic main group.  Second level of the code.
	 * 
	 * @return therapeutic main group
	 */
	public final int[]
	therapeuticMainGroup()
	{
		return this.secondLevel();
	}
	
	/**
	 * Therapeutic sub-group.  Third level of the code.
	 * 
	 * @return therapeutic sub-group
	 */
	public final char
	therapeuticSubGroup()
	{
		return this.thirdLevel();
	}
	
	/**
	 * Chemical sub-group.  Fourth level of the code.
	 * 
	 * @return chemical sub-group
	 */
	public final char
	chemicalSubGroup()
	{
		return this.fourthLevel();
	}
	
	/**
	 * Chemical substance. Fifth level.
	 * 
	 * @return chemical substance
	 */
	public final int[]
	chemicalSubstance()
	{
		return this.fifthLevel();
	}
	
	/**
	 * First level of the code, delegates to {@link #anatomicalMainGroup()}.
	 * 
	 * @return anatomical main group
	 */
	public final AnatomicalMainGroup
	firstLevel()
	{
		return this.firstLevel;
	}

	/**
	 * Second level of the code, delegates to {@link #theraputicMainGroup()}.
	 * 
	 * @return theraputic main group
	 */
	public final int[] 
	secondLevel()
	{
		return Arrays.copyOf(this.secondLevel, this.secondLevel.length);
	}

	/**
	 * Third level of the code, delegates to {@link #theraputicSubGroup()}.
	 * 
	 * @return theraputic sub-group
	 */
	public final char 
	thirdLevel()
	{
		return this.thirdLevel;
	}
	
	/**
	 * Fourth level of the code, delegates to {@link #chemicalSubGroup()}.
	 * 
	 * @return chemical sub-group
	 */
	public final char 
	fourthLevel()
	{
		return this.fourthLevel;
	}

	/**
	 * Fifth level of the code, delegates to {@link #chemicalSubstance()}.
	 * 
	 * @return chemical substance
	 */
	public final int[] 
	fifthLevel()
	{
		return Arrays.copyOf(this.fifthLevel, this.fifthLevel.length);
	}
	
	/**
	 * Attempt to parse the given input as an ATC code.
	 * 
	 * @param str string to parse
	 * @return parsed code, or <code>null</code> if the input was not a 
	 * valid ATC code
	 */
	public static final ATCCode
	parse(String str)
	{
		final String code = StringUtils.trimToEmpty(str).toUpperCase();
		
		if (code.length() != 7)
		{
			return null;
		}
		
		/* regex is expensive, but fast enough for our usage */
		if (! PATTERN.matcher(code).matches())
		{
			return null;
		}
		
		
		final char[] chars = code.toCharArray();
		
		final AnatomicalMainGroup main = 
				AnatomicalMainGroup.valueOf(chars[0]);
		
		if (main == null)
		{
			return null;
		}
		
		/* second level */
		final int[] second = new int[2];
		
		second[0] = chars[1] - '0';
		second[1] = chars[2] - '0';
		
		char third = chars[3];
		
		char fourth = chars[4];
		
		final int[] fifth = new int[2];
		
		fifth[0] = chars[5] - '0';
		fifth[1] = chars[6] - '0';
		
		return new ATCCode(main, second, third, fourth, fifth);
	}
	

	@Override
	public String
	toString()
	{
		return String.valueOf(this.firstLevel().code())
				+ String.valueOf(this.secondLevel())
				+ this.thirdLevel()
				+ this.fourthLevel()
				+ String.valueOf(this.fifthLevel());
	}

	/**
	 * The anatomical main group (first level).  Consists of a single
	 * letter.
	 * 
	 * @author Rob Tomsick (rtomsick@unc.edu)
	 *
	 */
	public static enum AnatomicalMainGroup
	{
		GI_METABOLISM('A'),
		BLOOD('B'),
		CARDIOVASCULAR('C'),
		DERMATOLOGICALS('D'),
		GENITO_URINARY('G'),
		HORMONAL('H'),
		SYSTEMIC_ANTIINFECTIVES('J'),
		ANTINEOPLASTIC_IMMUNOMODULATING('L'),
		MUSCULO_SKELETAL('M'),
		NERVOUS('N'),
		ANTIPARISITIC('P'),
		RESPIRATORY('R'),
		SENSORY('S'),
		VARIOUS('V');
		
		private final char		code;
		
		public char
		code()
		{
			return this.code;
		}
		
		public static AnatomicalMainGroup
		valueOf(char code)
		{
			for (AnatomicalMainGroup a : AnatomicalMainGroup.values())
			{
				if (code == a.code())
				{
					return a;
				}
			}
			
			return null;
		}
		
		AnatomicalMainGroup(char code)
		{
			this.code = code;
		}
	}
}
