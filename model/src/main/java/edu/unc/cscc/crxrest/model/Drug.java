/*-
 * ========================LICENSE_START=================================
 * model
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.crxrest.model;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * Represents a drug within CRxREST.  A drug is a pharmaceutical agent or 
 * ingredient and NOT the common consumer definition of a drug as a retail 
 * product.
 * </p>
 * 
 * <p>
 * This interface specifies a baseline of information that a dictionary may 
 * provide about a drug.  It is <b>not</b> an exhaustive definition of all 
 * properties that may be provided; implementations may contain additional
 * properties as required.
 * </p>
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 */
@JsonTypeName("drug")
@ApiModel(value = "drug", 
			description = "Represents a drug within CRxREST.  A drug is a "
					+ "pharmaceutical agent or ingredient.  Note that this is "
					+ "NOT the common consumer definition of a drug as a "
					+ "retail product or single physical object.  This "
					+ "model specifies a baseline of information that a "
					+ "dictionary may provide about a drug; dictionaries may "
					+ "extend this model with additional properties as "
					+ "required.")	
public final class Drug
{
	private final String		formula;
	private final String		name;
	private final Set<String>	synonyms;
	private final String active_numerator_strength;
	private final String active_ingred_unit;
	private final String dosage_form;
	
	/**
	 * Create a new drug with a given canonical name.  The resulting drug
	 * will not specify a chemical formula or any synonyms.
	 * 
	 * @param name name of the drug, not <code>null</code>, &ge; 1 characters
	 */
	public Drug(String name)
	{
		this(name, null, null);
	}
	
	/**
	 * Create a new drug with a given canonical name and a given chemical 
	 * formula.  The resulting drug will not specify any synonyms.
	 * 
	 * @param name name of the drug, not <code>null</code>, &ge; 1 characters
	 * @param chemicalFormula formula of the drug, may be <code>null</code>
	 * if unknown
	 */
	public Drug(String name, String chemicalFormula)
	{
		this(name, chemicalFormula, null);
	}
	
	/**
	 * Create a new drug with a given canonical name, a given chemical formula,
	 * and specifying a set of synonyms.
	 * 
	 * @param name name of the drug, not <code>null</code>, &ge; 1 characters
	 * @param checmicalFormula formula of the drug, may be <code>null</code>
	 * if unknown
	 * @param synonyms {@link #synonyms() synonyms} for the drug, may be 
	 * null or empty if no synonyms are known
	 */
	@JsonCreator
	public Drug(@JsonProperty("name") String name, 
				@JsonProperty("formula") String checmicalFormula, 
				@JsonProperty("synonyms") Set<String> synonyms)
	{
		this(name, checmicalFormula, synonyms, null, null, null);
	}
	
	
	/**
	 * Create a new drug with a given canonical name, a given chemical formula,
	 * given set of synonyms, a given active numerator strength, 
	 * a given active ingredient unit and a given dosage form
	 * @param name name if the drug
	 * @param checmicalFormula formula of the drug
	 * @param synonyms synonyms for the drug
	 * @param active_numerator_strength strength of the drug
	 * @param active_ingred_unit active ingredient of the drug
	 * @param dosage_form dosage form of the drug
	 */
	@JsonCreator
	public Drug(@JsonProperty("name") String name, 
				@JsonProperty("formula") String checmicalFormula, 
				@JsonProperty("synonyms") Set<String> synonyms, 
				@JsonProperty("active_numerator_strength") String active_numerator_strength, 
				@JsonProperty("active_ingred_unit") String active_ingred_unit, 
				@JsonProperty("dosage_form") String dosage_form)
	{
		this.formula = StringUtils.trimToNull(checmicalFormula) == null 
							? null 
							: checmicalFormula;
		this.name = name;
		this.synonyms = synonyms == null
							? Collections.emptySet()
							: new HashSet<>(synonyms);
							
		this.active_numerator_strength = active_numerator_strength;
		this.active_ingred_unit = active_ingred_unit;
		this.dosage_form = dosage_form;
	}	
	
	
	/**
	 * The canonical, user-facing name of the drug.  Required for all drugs.
	 * 
	 * @return user-facing canonical name, not <code>null</code>
	 */
	@NotNull
	@Size(min = 1)
	@JsonGetter("name")
	@ApiModelProperty(value = "The canonical, user-facing name of the drug.",
						example = "ibuprofen")
	public String
	canonicalName()
	{
		return this.name;
	}

	/**
	 * Active Numerator Strength of the Drug
	 * @return active numerator strength of the drug
	 */
	@NotNull
	@JsonGetter("active_numerator_strength")
	@ApiModelProperty(value = "active numerator strength",
						example = "active_numerator_strength")
	public String
	active_numerator_strength()
	{
		return this.active_numerator_strength;
	}
	
	/**
	 * Active Ingredient Unit of the Drug
	 * @return active ingredient unit of the drug
	 */
	@NotNull
	@JsonGetter("active_ingred_unit")
	@ApiModelProperty(value = "active ingred unit",
						example = "active ingred unit")
	public String
	active_ingred_unit()
	{
		return this.active_ingred_unit;
	}
	
	/**
	 * Dosage Form of the Drug
	 * @return dosage form of the drug
	 */
	@NotNull
	@JsonGetter("dosage_form")
	@ApiModelProperty(value = "dosage form",
						example = "dosage_form")
	public String
	dosage_form()
	{
		return this.dosage_form;
	}
	
	/**
	 * Display string intended to be used in the client search results. 
	 * This includes name, strength, unit and dosage-form of the drug
	 * @return display name 
	 */
	@JsonGetter("display-name")
	public String
	displayName()
	{
		return this.name
				+ " [Strength: " + this.active_numerator_strength + "] "
				+ " [Unit: " + this.active_ingred_unit + "] "
				+ " [Dosage Form: " + this.dosage_form + "]";
	}	
	
	/**
	 * The set of synonyms for the drug.  Does NOT include any trade names, but
	 * may include INN, USAN, JAN, etc.
	 * 
	 * @return synonyms, or empty if no synonyms exist
	 */
	@NotNull
	@JsonGetter("synonyms")
	@ApiModelProperty(value = "The set of synonyms for the drug.  Does NOT "
							+ "include any trade names, but may include INN, "
							+ "USAN, JAN, etc.")
	public Set<String> 
	synonyms()
	{
		return this.synonyms;
	}
	
	/**
	 * The chemical formula of the drug (if known.)
	 * 
	 * @return chemical formula, or <code>null</code> if unknown
	 */
	@Size(min = 1)
	@JsonGetter("formula")
	@ApiModelProperty(value = "The chemical formula of the drug (if known.)",
						example = "C13H18O2", allowEmptyValue = false,
						required = false)
	public String 
	chemicalFormula()
	{
		return this.formula;
	}

	@Override
	public int
	hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((formula == null) ? 0 : formula.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((synonyms == null) ? 0 : synonyms.hashCode());
		return result;
	}

	@Override
	public boolean 
	equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Drug other = (Drug) obj;
		if (formula == null)
		{
			if (other.formula != null)
				return false;
		}
		else if (!formula.equals(other.formula))
			return false;
		if (name == null)
		{
			if (other.name != null)
				return false;
		}
		else if (!name.equals(other.name))
			return false;
		if (active_numerator_strength == null)
		{
			if (other.active_numerator_strength != null)
				return false;
		}
		else if (!active_numerator_strength.equals(other.active_numerator_strength))
			return false;
		if (active_ingred_unit == null)
		{
			if (other.active_ingred_unit != null)
				return false;
		}
		else if (!active_ingred_unit.equals(other.active_ingred_unit))
			return false;
		if (dosage_form == null)
		{
			if (other.dosage_form != null)
				return false;
		}
		else if (!dosage_form.equals(other.dosage_form))
			return false;		
		
		if (synonyms == null)
		{
			if (other.synonyms != null)
				return false;
		}
		else
		{
			if (synonyms.size() != other.synonyms().size())
			{
				return false;
			}
			if (! synonyms.containsAll(other.synonyms()))
			{
				return false;
			}
			if (! other.synonyms().containsAll(synonyms))
			{
				return false;
			}
		}
		return true;
	}
	
	
	
}
