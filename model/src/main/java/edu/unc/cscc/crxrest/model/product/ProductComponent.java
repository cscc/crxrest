/*-
 * ========================LICENSE_START=================================
 * model
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.crxrest.model.product;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonTypeName;

import edu.unc.cscc.crxrest.model.Dose;
import edu.unc.cscc.crxrest.model.Drug;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * A product component is a tuple identifying a drug and a dose unit of that
 * drug.
 *  *
 */
@JsonTypeName("product-component")
@ApiModel(value = "product-component", 
		description = "A tuple identifying a drug and a dose of that drug.")
public final class ProductComponent
{	
	private final Drug		drug;
	private final Dose		dose;

	/**
	 * Create a new component consisting of a given dose of a given drug.
	 * 
	 * @param drug drug, not <code>null</code>
	 * @param dose dose of drug, not <code>null</code>
	 */
	@JsonCreator
	public ProductComponent(@JsonProperty("drug") Drug drug, 
							@JsonProperty("dose") Dose dose)
	{
		if (drug == null)
		{
			throw new IllegalArgumentException("drug may not be null");
		}
		
		if (dose == null)
		{
			throw new IllegalArgumentException("dose may not be null");
		}
		
		this.drug = drug;
		this.dose = dose;
	}
	
	/**
	 * The dose of the drug which makes up this component.
	 * 
	 * @return dose, not <code>null</code>
	 */
	@NotNull
	@JsonGetter("dose")
	@ApiModelProperty("The dose of the drug which makes up this component.")
	public Dose 
	dose()
	{
		return this.dose;
	}
	
	/**
	 * The drug which makes up this component.
	 * 
	 * @return drug, not <code>null</code>
	 */
	@NotNull
	@JsonGetter("drug")
	@ApiModelProperty("The drug which makes up this component.")
	public Drug 
	drug()
	{
		return this.drug;
	}
	
	/* Swagger hacks below */
	
	@JsonSetter("drug")
	private void
	HACK_setDrug(Drug drug)
	{
	}
	
	@JsonSetter("dose")
	private void
	HACK_setDose(Dose dose)
	{
	}
}
