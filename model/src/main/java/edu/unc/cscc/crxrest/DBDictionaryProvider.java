/*-
 * ========================LICENSE_START=================================
 * model
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.crxrest;

import java.io.IOException;
import java.io.InputStream;

import javax.sql.DataSource;

/**
 * <p>
 * A {@link DictionaryProvider dictionary provider} which requires a temporary
 * database for initialization/usage.  The database will be managed by CRxREST,
 * and may be used by the provider in any way it pleases.</p>
 * 
 * <p>While it is not required that implementations make use of the 
 * provided data source, implementations that will never need said database
 * should implement {@link SimpleDictionaryProvider} instead.</p>
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
public interface DBDictionaryProvider
extends DictionaryProvider
{
	/**
	 * The type of database required by the provider.  The data source supplied
	 * to {@link #initialize(DataSource)} will correspond to a database of this
	 * type.  Defaults to {@link DBType#SQLITE}.
	 * 
	 * @return database type, not <code>null</code>
	 */
	public default DBType
	requiredType()
	{
		return DBType.SQLITE;
	}
	
	/**
	 * <p>
	 * Initialization method.  Dictionary providers may implement this method
	 * to do any one-time initializations (such as checking for external 
	 * resources) on startup.</p>
	 * 
	 * <p>Implementations will be given a {@link DataSource data source} 
	 * corresponding to a database that 
	 * is unique to their implementation, the management of which is handled by 
	 * by CRxREST.  If new, the data source may be initialized with the 
	 * {@link #schemaStream() provided schema} prior to being given to this 
	 * method. The data source may persist across multiple runs of
	 * CRxREST, depending on 
	 * {@link DBDictionaryProvider#persistenceID() the provider's support} 
	 * for this feature</p>
	 * 
	 * <p>Implementations may throw any 
	 * {@link Throwable} in the event of an error during initialization 
	 * (for example: a required external dataset file is not found)
	 * </p>
	 * 
	 * <p>
	 * The default implementation of this method does nothing.
	 * </p>
	 * 
	 * @param ds data source, not <code>null</code>
	 * @param newDataSource <code>true</code> if the given data source is 
	 * newly-created (i.e. has not previously been used for initialization),
	 * <code>false</code> otherwise
	 */
	public default void initialize(DataSource ds, boolean newDataSource)
	throws Throwable
	{
		/* do nothing */
	}
	
	/**
	 * <p>
	 * ID used to identify persisted data sources across executions.  If no ID
	 * is produced, persistence will not be supported for this provider.  By 
	 * default, the canonical class name of the provider implementation will be
	 * used as part of the ID.  Implementations for which this is not 
	 * sufficient are encouraged to override the default implementation.
	 * </p>
	 * 
	 * <p>This ID is intended to be both stable and unique to an instance of 
	 * a provider implementation.</p>
	 * 
	 * <p>
	 * Supporting persistence does <b>NOT</b> mean that initialization will 
	 * be done only one time.  External circumstances (such as removal) of 
	 * temporary files may cause deletion of a persisted data source.  
	 * Therefore providers which are capable of initializing their own 
	 * data sources must capable of doing so at any time.
	 * </p>
	 * 
	 * @return persistence ID, or <code>null</code> if persistence is not 
	 * supported
	 */
	public default String persistenceID()
	{
		return "DB-" + this.getClass().getCanonicalName();
	}
	
	/**
	 * Get the schema which should be used to initialize the datasource 
	 * prior to usage by this provider.  The returned stream will be closed
	 * after use by CRxREST, and should not be retained or reused by the 
	 * provider.
	 * 
	 * @return stream providing access to the schema
	 * @throws IOException may be thrown if a stream of the schema could not 
	 * be opened
	 */
	public InputStream schemaStream() throws IOException;
	
	/**
	 * Supported database types.
	 * 
	 */
	public static enum DBType
	{
		HSQL,
		SQLITE;
	}

}
