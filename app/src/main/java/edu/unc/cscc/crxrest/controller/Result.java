package edu.unc.cscc.crxrest.controller;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonTypeName;

import edu.unc.cscc.crxrest.model.Drug;
import edu.unc.cscc.crxrest.model.Simple;
import edu.unc.cscc.crxrest.model.TradeName;
import edu.unc.cscc.crxrest.model.product.Product;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * A search result, as returned in response to a search for a {@link Product},
 * {@link TradeName}, {@link Drug}, etc.
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 * @param <T> type of the result object
 */
/* Note: this class *could* be fully generic with no sub-classes if not for 
 * Swagger.  Unfortunately, there's no way to get Swagger to see the result
 * type correctly if we do that, so we have sub-classes here to ensure type
 * visibility, and to provide entity-specific doc overrides
 */
@ApiModel("result")
@JsonTypeName("result")
public class Result<T> 
{	
	private final String		name;
	protected final T			result;
	
	Result(String name, T result)
	{
		this.name = name;
		this.result = result;
	}

	@JsonGetter("display-name")
	@ApiModelProperty(value = "A human-readable display string representing "
							+ "the result.",
						example = "Ibuprofen 200mg, tablet")
	public String
	displayName()
	{
		return this.name;
	}

	@JsonGetter("result")
	@ApiModelProperty(value = "The search result itself")
	public T
	result()
	{
		return this.result;
	}
	
	public static final  <Z> Result<Z>
	of(Z result, String name)
	{
		return new Result<>(name, result);
	}
	
	@ApiModel("product-result")
	static final class ProductResult
	extends Result<Product>
	{

		ProductResult(Product product)
		{
			super(product.displayName(), product);
		}
		
		@ApiModelProperty(value = "A human-readable display string representing "
				+ "the product.",
			example = "Equat Ibuprofen 200 mg tablet")
		public String
		displayName()
		{
			return super.displayName();
		}
	}
	
	@ApiModel("tradename-result")
	static final class TradeNameResult
	extends Result<TradeName>
	{
		TradeNameResult(TradeName name)
		{
			super(name.name(), name);
		}
		
		@ApiModelProperty(value = "A human-readable display string representing "
				+ "the trade name.",
			example = "Motrin")
		public String
		displayName()
		{
			return super.displayName();
		}
	}

	@ApiModel("drug-result")
	static final class DrugResult
	extends Result<Drug>
	{
		DrugResult(Drug drug)
		{
			super(drug.canonicalName(), drug);
		}
		
		@ApiModelProperty(value = "A human-readable display string representing "
								+ "the drug.",
							example = "ibuprofen")
		public String
		displayName()
		{
			return super.displayName();
		}
	}
	
	@ApiModel("simple-result")
	static final class SimpleResult
	extends Result<Simple>
	{

		SimpleResult(Simple simple)
		{
			super(simple.name(), simple);
		}
		
		@ApiModelProperty(value = "A human-readable display string representing "
				+ "the Simple Result.",
			example = "Engineer")
		public String
		displayName()
		{
			return result.displayName();
		}
	}
}
