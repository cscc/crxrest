package edu.unc.cscc.crxrest.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.unc.cscc.crxrest.Dictionary;
import edu.unc.cscc.crxrest.Dictionary.DictionaryCapabilities;
import edu.unc.cscc.crxrest.DictionaryService;
import edu.unc.cscc.crxrest.config.DictionaryProviderSource;
import edu.unc.cscc.crxrest.controller.Result.DrugResult;
import edu.unc.cscc.crxrest.controller.Result.ProductResult;
import edu.unc.cscc.crxrest.controller.Result.SimpleResult;
import edu.unc.cscc.crxrest.controller.Result.TradeNameResult;
import edu.unc.cscc.crxrest.model.NDC;
import edu.unc.cscc.crxrest.model.Simple;
import edu.unc.cscc.crxrest.model.barcode.Barcode;
import edu.unc.cscc.crxrest.model.barcode.Barcode.MalformedBarcodeException;
import edu.unc.cscc.crxrest.model.barcode.UPCA;
import edu.unc.cscc.crxrest.model.product.Product;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api("search")
@RestController
@RequestMapping("/dictionaries/")
@Validated
public class SearchController
{
	
	@Autowired
	private DictionaryProviderSource		dpSource;

	
	@RequestMapping(value = "/{dictionary}/products",
					method = RequestMethod.GET,
					produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "free-text search within a dictionary for products",
					notes = "Search within a given dictionary for products "
							+ "matching the given search query.  The semantics "
							+ "of \"matching\" are left up to the underlying "
							+ "dictionary service.")
	@ApiResponses({
		@ApiResponse(code = 200,
						message = "search completed successfully",
						response = Result.ProductResult.class,
						responseContainer = "List"),
		@ApiResponse(code = 404, message = "dictionary not found")
	})
	public ResponseEntity<List<Result.ProductResult>>
	searchProducts(@RequestParam(
						value = "q",
						required = true)
					@ApiParam(name = "q", value = "search term",
								allowEmptyValue = false)
					@NotNull @Size(min = 1)
						String query, 
						
					@PathVariable("dictionary")
					@ApiParam(value = "name of dictionary to search")
					@NotNull @Size(min = 1)
						String dictionaryName,
						
					@RequestParam(value = "limit",
									required = false, 
									defaultValue = "100")
					@ApiParam(value = "maximum number of results to produce, "
									+ "may be overridden by underlying "
									+ "dictionary implementation",
								defaultValue = "100")
					@Min(1) @Max(1000)
						int limit)
	{
		
		/* resolve dictionary */
		final DictionaryService service = this.dpSource.get(dictionaryName);
		
		if (service == null)
		{
			return ResponseEntity.notFound().build();
		}
		
		limit = service.resultLimit() < limit ? service.resultLimit() : limit;
		
		List<Result.ProductResult> results = 
				service.findProducts(query, limit)
						.stream()
						.map(Result.ProductResult :: new)
						.collect(Collectors.toList());
		
		return ResponseEntity.ok(results);
	}
	
	@RequestMapping(value = "/{dictionary}/products/by-ndc",
					method = RequestMethod.GET, 
					produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "search for products by NDC",
					notes = "Search for any products with the given NDC.  If a "
							+ "partial NDC is supplied, any products with an NDC "
							+ "containing the same NDC code groupings "
							+ "(labeller and product) may be found. If the "
							+ "dictionary does not support NDCs, this operation "
							+ "will produce no results.")
	@ApiResponses({
		@ApiResponse(code = 200, message = "search completed successfully",
						response = ProductResult.class,
						responseContainer = "List"),
		@ApiResponse(code = 204, message = "dictionary did not support NDC",
						response = void.class),
		@ApiResponse(code = 404, message = "dictionary not found")
	})
	public ResponseEntity<List<ProductResult>>
	searchByNDC(@RequestParam(
							value = "ndc",
							required = true)
						@NotNull
						@ApiParam(value = "NDC by which to search", 
									type = "string", 
									example = "49035-248-05", required = true)	
							NDC ndc, 
							
						@PathVariable("dictionary")
						@NotNull @Size(min = 1)
						@ApiParam("name of the dictionary to search")
							String dictionaryName,
							
						@RequestParam(value = "limit",
										required = false, 
										defaultValue = "100")
						@Min(1) @Max(1000)
						@ApiParam(value = "maximum number of results to produce, "
										+ "may be overridden by underlying "
										+ "dictionary implementation",
								defaultValue = "100")
							int limit)
	{
		final DictionaryService service = this.dpSource.get(dictionaryName);
		
		if (service == null)
		{
			return ResponseEntity.notFound().build();
		}
		
		limit = service.resultLimit() < limit ? service.resultLimit() : limit;
		
		/* check that the dictionary supports NDC, if not we're done */
		if (! service.dictionary()
					.capabilities().contains(DictionaryCapabilities.NDC))
		{
			return ResponseEntity.noContent().build();
		}
		
		final List<ProductResult> results = 
				service.findByNDC(ndc, limit)
						.stream()
						.map(ProductResult :: new)
						.collect(Collectors.toList());
		
		return ResponseEntity.ok(results);
	}
	
	@RequestMapping(value = "/{dictionary}/tradenames",
					method = RequestMethod.GET, 
					produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "search for trade names",
					notes = "Search for trade names matching the given query.  "
							+ "The semantics of \"matching\" are left up to "
							+ "the dictionary.")
	@ApiResponses({
		@ApiResponse(code = 200, 
						message = "search completed successfully",
						response = TradeNameResult.class,
						responseContainer = "List"),
		@ApiResponse(code = 204, 
						message = "dictionary did not support trade names",
						response = void.class),
		@ApiResponse(code = 404, message = "dictionary not found")
	})
	public ResponseEntity<List<TradeNameResult>>
	searchTradeNames(@RequestParam(
							value = "q",
							required = true)
						@ApiParam(value = "search term", required = true, 
									allowEmptyValue = false)
						@NotNull @Size(min = 1)
							String query, 
							
						@PathVariable("dictionary")
						@ApiParam(value = "name of the dictionary to search")
						@NotNull @Size(min = 1)
							String dictionaryName,
							
						@RequestParam(value = "limit",
										required = false, 
										defaultValue = "100")
						@ApiParam(value = "maximum number of results to produce, "
								+ "may be overridden by underlying "
								+ "dictionary implementation",
								defaultValue = "100")
						@Min(1) @Max(1000)
							int limit)
	{
		
		final DictionaryService service = this.dpSource.get(dictionaryName);
		
		if (service == null)
		{
			return ResponseEntity.notFound().build();
		}
		
		limit = service.resultLimit() < limit ? service.resultLimit() : limit;
		
		if (! service.dictionary().supports(DictionaryCapabilities.TRADE_NAMES))
		{
			return ResponseEntity.noContent().build();
		}
		
		return ResponseEntity
				.ok(service.findTradeNames(query, limit)
							.stream()
							.map(TradeNameResult :: new)
							.collect(Collectors.toList()));
	}
	
	@ApiOperation(value = "search for drugs",
			notes = "Search for drugs matching the given query.  "
					+ "The semantics of \"matching\" are left up to "
						+ "the dictionary.")
	@ApiResponses({
		@ApiResponse(code = 200, 
						message = "search completed successfully",
						response = DrugResult.class,
						responseContainer = "List"),
		@ApiResponse(code = 404, message = "dictionary not found")
	})
	@RequestMapping(value = "/{dictionary}/drugs",
					method = RequestMethod.GET, 
					produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<DrugResult>>
	searchDrugs(@RequestParam(
					value = "q",
					required = true)
				@ApiParam(value = "search term", required = true, 
							allowEmptyValue = false)
				@NotNull @Size(min = 1)
					String query, 
					
				@PathVariable("dictionary")
				@ApiParam(value = "name of the dictionary to search")
				@NotNull @Size(min = 1)
					String dictionaryName,
					
				@RequestParam(value = "limit",
								required = false, 
								defaultValue = "100")
				@ApiParam(value = "maximum number of results to produce, "
								+ "may be overridden by underlying "
								+ "dictionary implementation",
							defaultValue = "100")
				@Min(1) @Max(1000)
					int limit)
	{
		final DictionaryService service = this.dpSource.get(dictionaryName);
		
		if (service == null)
		{
			return ResponseEntity.notFound().build();
		}
		
		limit = service.resultLimit() < limit ? service.resultLimit() : limit;
		
		final List<DrugResult> results = 
				service.findDrugs(query, limit)
						.stream()
						.map(DrugResult :: new)
						.collect(Collectors.toList());
		
		return ResponseEntity.ok(results);
	}
	
	/* TODO - when we support image rec, this endpoint will be overloaded
	 * to allow for barcode to be a POSTed image or something
	 */
	@RequestMapping(value = "/{dictionary}/products/by-barcode",
					method = RequestMethod.GET, 
					produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ProductResult>>
	searchByBarcode(@RequestParam(
						value = "barcode",
						required = false,
						defaultValue = "")
					@NotNull
						String barcodeStr, 

					@RequestParam(value = "infer-ndc", required = false, 
									defaultValue = "false")
					boolean inferNDC,
						
					@PathVariable("dictionary")
					@NotNull @Size(min = 1)
						String dictionaryName,
						
					@RequestParam(value = "limit",
									required = false, 
									defaultValue = "100")
					@Min(1)
						int limit)
	{
		final DictionaryService service = this.dpSource.get(dictionaryName);
		
		if (service == null)
		{
			return ResponseEntity.notFound().build();
		}
		
		limit = service.resultLimit() < limit ? service.resultLimit() : limit;
		
		List<Product> results = new ArrayList<>();
		
		/* try to figure out what barcode it is... */
		final Barcode<?> barcode = extractBarcode(barcodeStr);
		
		if (barcode == null)
		{
			return ResponseEntity.ok(Collections.emptyList());
		}
		if (service.dictionary().supports(DictionaryCapabilities.BARCODES))
		{
			results.addAll(service.findByBarcode(barcode, limit));
		}
		
		/* If we have a UPCA, see if we can get a UPN out of it and search by
		 * NDC that way.
		 */
		if (service.dictionary().supports(DictionaryCapabilities.NDC) 
			&& barcode instanceof UPCA
			&& results.isEmpty()
			&& inferNDC)
		{
			/* handle UPN if available */
			if (barcode instanceof UPCA)
			{
				NDC[] ndcs = NDC.possibleNDCs((UPCA) barcode);
				
				for (NDC n : ndcs)
				{
					int max = results.size() - limit;
					max = max < 0 ? limit : max;
					
					results.addAll(service.findByNDC(n, max));
				}
			}
		}
		
		return ResponseEntity.ok(results.stream()
										.map(ProductResult :: new)
										.collect(Collectors.toList()));
	}
	
	@RequestMapping(value = "/{dictionary}/all",
					method = RequestMethod.GET, 
					produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "search for entities of all types",
					notes = "Multi-entity search.  Search products, drugs, etc. "
							+ "matching a given query.  The returned result list "
							+ "may contain a mixture of entity types.  You should "
							+ "use this endpoint only when multiple requests "
							+ "(one for each entity type) are not feasible.")
	@ApiResponses({
		@ApiResponse(code = 200, message = "search completed successfully",
						response = Result.class,
						responseContainer = "List"),
		@ApiResponse(code = 404, message = "dictionary not found")
	})
	public ResponseEntity<List<Result<?>>>
	searchAll(@RequestParam(
					value = "q",
					required = true)
				@ApiParam(value = "search term", required = true, 
							allowEmptyValue = false)
				@NotNull @Size(min = 1)
					String query, 
					
				@PathVariable("dictionary")
				@ApiParam(value = "name of the dictionary to search")
				@NotNull @Size(min = 1)
					String dictionaryName,
					
				@RequestParam(value = "limit",
								required = false, 
								defaultValue = "100")
				@ApiParam(value = "maximum number of results to produce, "
								+ "may be overridden by underlying "
								+ "dictionary implementation",
							defaultValue = "100")
				@Min(1) @Max(1000)
					int limit,
				
				@RequestParam(name = "search-ndc", defaultValue = "true")
				@ApiParam(value = "whether to search by NDC; if true, CRxREST "
						+ "will attempt to parse the search term as an NDC, and "
						+ "if successful will search for products with the NDC",
						required = false, defaultValue = "true")
					boolean searchNDC,
					
				@RequestParam(name = "search-barcodes", defaultValue = "true")
				@ApiParam(value = "whether to search by barcode; if true, CRxREST "
						+ "will check if the search term can be interpreted as a "
						+ "barcode, and if so will search for products with "
						+ "the barcode",
						required = false, defaultValue = "true")
					boolean searchBarcodes,
					
				@RequestParam(name = "search-trade-names", defaultValue = "false")
				@ApiParam(value = "whether search for trade names matching the "
								+ "search term",
						required = false, defaultValue = "false")
					boolean searchTradeNames,
				
				@RequestParam(name = "search-product-names", defaultValue = "true")
				@ApiParam(value = "whether to search for product names matching "
								+ "the search term", required = false,
								defaultValue = "true")
					boolean searchProductNames,
					
				@RequestParam(name = "search-drugs", defaultValue = "false")
				@ApiParam(value = "whether to search for drugs matching "
								+ "the search term",
							defaultValue = "false")
					boolean searchDrugs,
				
				@RequestParam(value = "infer-ndc", required = false, 
								defaultValue = "true")
				@ApiParam(value = "Whether to attempt to infer NDCs usable "
									+ "for search from UPC-A barcodes.  Due "
									+ "to the inherent ambiguity of UPC's " 
									+ "representation of NDCs, this may "
									+ "produce inexact or inaccurate results.",
							defaultValue = "true", required = false)
					boolean inferNDC)
	{
		final DictionaryService service = this.dpSource.get(dictionaryName);
		
		if (service == null)
		{
			return ResponseEntity.notFound().build();
		}
		
		limit = service.resultLimit() < limit ? service.resultLimit() : limit;
		
		final Dictionary dictionary = service.dictionary();
		
		final String strippedQuery = StringUtils.stripToEmpty(query);
		
		/* override flags based on dictionary caps */
		searchNDC = 
				searchNDC && dictionary.supports(DictionaryCapabilities.NDC);
		searchBarcodes = 
				searchBarcodes && dictionary.supports(DictionaryCapabilities.BARCODES);
		
		
		NDC ndc = null;
		Barcode<?> barcode = null;
		
		/* if it's > 13 chars, then it can't be an NDC, even with 
		 * separator chars
		 */
		if (searchNDC && strippedQuery.length() <= 13)
		{
			try
			{
				ndc = NDC.parse(strippedQuery);
			}
			catch (IllegalArgumentException e)
			{
				ndc = null;
			}
		}
		
		barcode = searchBarcodes ? extractBarcode(strippedQuery) : null;

		List<Result<?>> results = new ArrayList<>();
		
		if (ndc != null)
		{
			results.addAll(service.findByNDC(ndc, limit)
								.stream()
								.map(p -> Result.of(p, p.displayName()))
								.collect(Collectors.toList()));
			
			if (results.size() >= limit)
			{
				return ResponseEntity.ok(results);
			}
		}
		
		int max = results.size() - limit;
		max = max < 0 ? limit : max;

		if (barcode != null)
		{
			results.addAll(service.findByBarcode(barcode, max)
					.stream()
					.map(p -> Result.of(p, p.displayName()))
					.collect(Collectors.toList()));

			if (results.size() >= limit)
			{
				return ResponseEntity.ok(results);
			}
			
			/* handle UPN if available */
			if (inferNDC && (barcode instanceof UPCA))
			{
				NDC[] ndcs = NDC.possibleNDCs((UPCA) barcode);
				
				for (NDC n : ndcs)
				{
					max = results.size() - limit;
					max = max < 0 ? limit : max;
					
					results.addAll(service.findByNDC(n, max)
							.stream()
							.map(p -> Result.of(p, p.displayName()))
							.collect(Collectors.toList()));
				}
			}
		}
		
		/* if either NDC or barcode got hits, early exit */
		if (ndc != null || barcode != null)
		{
			if (! results.isEmpty())
			{
				return ResponseEntity.ok(results);
			}
		}
		

		max = results.size() - limit;
		max = max < 0 ? limit : max;
		
		if (searchProductNames)
		{
			results.addAll(service.findProducts(strippedQuery, max)
					.stream()
					.map(p -> Result.of(p, p.displayName()))
					.collect(Collectors.toList()));

			if (results.size() >= limit)
			{
				return ResponseEntity.ok(results);
			}
			
			max = results.size() - limit;
			max = max < 0 ? limit : max;
		}

		if (searchDrugs)
		{
			/* search by drug name */
			
			results.addAll(service.findDrugs(strippedQuery, max)
					.stream()
					.map(p -> Result.of(p, p.displayName()))
					.collect(Collectors.toList()));

			if (results.size() >= limit)
			{
				return ResponseEntity.ok(results);
			}

			max = results.size() - limit;
			max = max < 0 ? limit : max;
		}
		
		/* finally, trade names if required */
		if (searchTradeNames)
		{
			results.addAll(service.findTradeNames(strippedQuery, max)
					.stream()
					.map(p -> Result.of(p, p.name()))
					.collect(Collectors.toList()));
		}
		
		return ResponseEntity.ok(results);
	}
	
	private static final Barcode<?>
	extractBarcode(String str)
	{
		try
		{
			return UPCA.parse(str);
		}
		catch (MalformedBarcodeException e) { }
		
		return null;
	}
	
	@RequestMapping(value = "/{dictionary}/by-name",
					method = RequestMethod.GET, 
					produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<SimpleResult>>
	searchByName(@RequestParam(
						value = "q",
						required = false,
						defaultValue = "")
					@NotNull
						String query, 
					@PathVariable("dictionary")
					@NotNull @Size(min = 1)
						String dictionaryName,
						
					@RequestParam(value = "limit",
									required = false, 
									defaultValue = "100")
					@Min(1)
						int limit)
	{
		final DictionaryService service = this.dpSource.get(dictionaryName);
		
		if (service == null)
		{
			return ResponseEntity.notFound().build();
		}
		
		limit = service.resultLimit() < limit ? service.resultLimit() : limit;
		
		List<Simple> results = new ArrayList<>();
		
		results.addAll(service.findByName(query, limit));
		
		return ResponseEntity.ok(results.stream()
										.map(SimpleResult :: new)
										.collect(Collectors.toList()));
	}
}
