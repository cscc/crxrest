package edu.unc.cscc.crxrest.config;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.unc.cscc.crxrest.DictionaryProvider;
import edu.unc.cscc.crxrest.DictionaryService;

public final class DictionaryProviderSource
{
	
	private final Map<String, DictionaryProvider>	providers;
	
	private final Logger							logger;
	
	DictionaryProviderSource(Collection<DictionaryProvider> providers)
	{
		this.providers = new HashMap<>();
		this.logger = LoggerFactory.getLogger("dictionary-providers");
		
		for (final DictionaryProvider provider : providers)
		{		
			Set<String> dictionaryNames = provider.providedDictionaries();
			
			for (String identifier : dictionaryNames)
			{
				if (this.providers.containsKey(identifier))
				{
					this.logger.warn(
							String.format("Duplicate dictionary identifier '%s', "
									+ "refusing to override", identifier));
					continue;
				}
				
				this.providers.put(identifier, provider);
			}
			
			this.logger.info(String.format(
					"Initialized provider for dictionaries: %s",
					dictionaryNames.stream().collect(Collectors.joining(","))));
		}
	}
	
	/**
	 * Get all of the identifiers of dictionary services that are known to 
	 * this source.
	 * 
	 * @return set of identifiers, not <code>null</code>
	 */
	public Set<String>
	enumerateIdentifiers()
	{
		return Collections.unmodifiableSet(this.providers.keySet());
	}


	/**
	 * Get the dictionary service for the dictionary with the given identifier.
	 * 
	 * @param identifier identifier of the dictionary for which to obtain a 
	 * service
	 * 
	 * @return dictionary service
	 */
	public DictionaryService
	get(String identifier)
	{
		if (this.providers.containsKey(identifier))
		{
			return this.providers.get(identifier).load(identifier);
		}
		
		return null;
	}
}
