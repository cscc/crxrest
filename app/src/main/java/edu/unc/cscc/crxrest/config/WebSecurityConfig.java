package edu.unc.cscc.crxrest.config;

import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, 
							proxyTargetClass = false,
							mode = AdviceMode.ASPECTJ)
public class WebSecurityConfig
extends WebSecurityConfigurerAdapter
{
	protected void 
    configure(HttpSecurity http) 
    throws Exception 
    {
        http.csrf().disable()
    		.authorizeRequests()
    		.antMatchers("/dictionaries/**")
    		.permitAll();
    }
}
