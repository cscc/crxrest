package edu.unc.cscc.crxrest.controller;

import java.util.Collection;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import edu.unc.cscc.crxrest.Dictionary;
import edu.unc.cscc.crxrest.DictionaryService;
import edu.unc.cscc.crxrest.config.DictionaryProviderSource;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api("dictionaries")
@RestController
@RequestMapping("/dictionaries")
public class DictionaryController
{
	
	@Autowired
	private DictionaryProviderSource		source;

	
	@RequestMapping(value = {"", "/"},
					method = RequestMethod.GET,
					produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "enumerate the available dictionaries and "
						+ "their properties",
					notes = "Get all of the dictionaries (and their properties) "
							+ "available for use.")
	@ApiResponses({
		@ApiResponse(code = 200, message = "dictionaries successfully retrieved")
	})
	public Collection<Dictionary>
	enumerate()
	{
		return this.source.enumerateIdentifiers()
							.stream()
							.map(i -> this.source.get(i).dictionary())
							.collect(Collectors.toSet());
	}
	
	@RequestMapping(value = "/{name}",
					method = RequestMethod.GET,
					consumes = MediaType.APPLICATION_JSON_VALUE,
					produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "get the properties of a dictionary",
					notes = "Get the properties of a specified dictionary.  The "
							+ "resulting properties may be used to determine "
							+ "which of the exposed endpoints should be used "
							+ "with the given dictionary, as well as license "
							+ "and other information about the dictionary.")
	@ApiResponses({
			@ApiResponse(code = 200, 
						message = "capabilities successfully retrieved",
						response = Dictionary.class),
			@ApiResponse(code = 404,
						message = "dictionary not found")
	})
	public ResponseEntity<Dictionary>
	capabilities(@PathVariable(value = "name")
					@NotNull @Size(min = 1)
					@ApiParam(value = "identifier of the dictionary")
						final String identifier)
	{
		
		DictionaryService service = this.source.get(identifier);
		
		if (service == null)
		{
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(service.dictionary());
	}
	
}
