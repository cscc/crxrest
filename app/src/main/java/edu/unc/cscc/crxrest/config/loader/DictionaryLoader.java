package edu.unc.cscc.crxrest.config.loader;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.unc.cscc.crxrest.DictionaryProvider;
import edu.unc.cscc.crxrest.Discoverable;
import io.github.lukehutch.fastclasspathscanner.FastClasspathScanner;
import io.github.lukehutch.fastclasspathscanner.scanner.ScanResult;


public final class DictionaryLoader
{
	
	private static final Logger		LOGGER = 
			LoggerFactory.getLogger(DictionaryLoader.class);

	/**
	 * Attempt to find any {@link DictionaryLoader dictionary providers}, 
	 * optionally using the given class loader for the search.  If no 
	 * class loader is provided, the default one will be used.
	 * 
	 * @param loader loader to use, or <code>null</code> to use the one 
	 * which loaded this class
	 * 
	 * @return all discovered providers, not <code>null</code>, may be 
	 * empty
	 * @throws IOException
	 */
	public static final Collection<DictionaryProvider>
	findProviders(ClassLoader loader)
	throws IOException
	{
		
		Collection<Class<? extends DictionaryProvider>> cls = 
				discoverImplementations(DictionaryProvider.class, 
										Discoverable.class, loader);
		
		Set<DictionaryProvider> providers = new HashSet<>();
		
		for (Class<? extends DictionaryProvider> c : cls)
		{
			try
			{
				LOGGER.info(String.format(
						"Trying to instantiate provider imple '%s'", 
						c.getCanonicalName()));
				providers.add(c.newInstance());
			}
			catch (InstantiationException | IllegalAccessException e)
			{
				LOGGER.error(String.format(
						"Failed to instantiate dictionary provider '%s'", 
						c.getCanonicalName()), e);
			}
		}
		
		return providers;
	}
	
	/**
	 * Discover implementations of the specified class that are annotated 
	 * with the given annotation.
	 * 
	 * @param cls interface for which to find implementations, not 
	 * <code>null</code>
	 * @param aCls annotation which must be present on implementations of the
	 * given class, or <code>null</code> if all implementations should be 
	 * found regardless of annotation
	 * @param loader classloader to use for loading classes, or <code>null</code>
	 *  to use the loader used to load the given class
	 * @return all discovered classes implementing the specified interface
	 *  
	 * @throws IOException
	 */
	public static final <T> Collection<Class<? extends T>> 
	discoverImplementations(Class<T> cls, Class<?> aCls, ClassLoader l)
	throws IOException
	{
		
		final ClassLoader loader = 
				(l == null) ? cls.getClassLoader() : l;
		
		final Set<String> classNames = new HashSet<>();
				
		FastClasspathScanner scanner = 
				new FastClasspathScanner("-org.springframework");
		
		if (l != null)
		{
			scanner.addClassLoader(loader);
		}
		
		ScanResult result = scanner.scan();
		
		/* get all implementations */
		classNames.addAll(result.getNamesOfClassesImplementing(cls));
		
		LOGGER.debug("Found implementing classes: \n" + 
					classNames.stream().collect(Collectors.joining(",")));
		
		if (aCls != null)
		{
			
			/* filter out non-annotated implementations */
			
			Set<String> haveAnnotations = 
					new HashSet<>(result.getNamesOfClassesWithAnnotation(aCls));
			
			classNames.removeIf(s -> ! haveAnnotations.contains(s));
		}
		LOGGER.debug("Implementing classes post-annotation filter: \n" + 
				classNames.stream().collect(Collectors.joining(",")));
		
				
		/* now instantiate all our classes */
		
		return classNames
				.stream()
				.map(t -> {
					try
					{
						return (Class<? extends T>) loader.loadClass(t).asSubclass(cls);
					}
					catch (ClassNotFoundException e)
					{
						throw new RuntimeException(e);
					}
				})
				.collect(Collectors.toList());	
	}
	
}
