package edu.unc.cscc.crxrest.config;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import edu.unc.cscc.crxrest.ConfigurableProvider;
import edu.unc.cscc.crxrest.DBDictionaryProvider;
import edu.unc.cscc.crxrest.DictionaryProvider;
import edu.unc.cscc.crxrest.SimpleDictionaryProvider;
import edu.unc.cscc.crxrest.config.loader.DictionaryLoader;

@Configuration
@ConfigurationProperties("crxrest.providers")
public class ProviderConfig 
implements InitializingBean
{
	
	private static final Logger            LOG = 
			LoggerFactory.getLogger("dictionary-config");
	
	private final List<String>             additionalPaths;
	private final Map<String, Properties>  providerProperties;
	private final Map<String, String>      providerConfigs;
	private boolean                        failIfNoneConfigured;

	public ProviderConfig()
	{
		this.additionalPaths = new ArrayList<>();
		this.providerProperties = new HashMap<>();
		this.providerConfigs = new HashMap<>();
	}
	
	public boolean 
	getFailIfNoneConfigured()
    {
        return failIfNoneConfigured;
    }

    public void 
    setFailIfNoneConfigured(boolean fail)
    {
        this.failIfNoneConfigured = fail;
    }

    public List<String> 
	getPaths()
    {
        return additionalPaths;
    }
	
	public void 
	setPaths(List<String> paths)
	{
	    this.additionalPaths.clear();
	    this.additionalPaths.addAll(paths);
	}
	
	public Map<String, String>
	getConfig()
	{
	    return this.providerConfigs;
	}

	public void 
	setConfig(Map<String, String> providerConfigs)
	{
	    this.providerConfigs.clear();
	    this.providerConfigs.putAll(providerConfigs);
	}


    @Bean
	public DictionaryProviderSource
	providerSource(DBConfig dbConfig)
	throws IOException
	{
		Collection<DictionaryProvider> providers = 
				DictionaryLoader.findProviders(this.createLoader());
		
		Set<String> providerImplNames = new HashSet<>();
		
		List<DictionaryProvider> uniqueProviders = new ArrayList<>();
		
		for (DictionaryProvider p : providers)
		{
			final String name = p.getClass().getCanonicalName();
			
			if (providerImplNames.contains(name))
			{
				LOG.warn(String.format(
						"More than one instance of provider implementation "
						+ "'%s' found, ignoring all but first", name));
				continue;
			}
			providerImplNames.add(name);
			uniqueProviders.add(p);
		}
		
		final List<DictionaryProvider> initialized = 
				uniqueProviders
					.parallelStream()
					.map(p -> initializeProvider(p, dbConfig))
					.filter(p -> p != null)
					.collect(Collectors.toList());
		
		if (initialized.isEmpty())
		{
		    final String baseMsg = 
		            "Failed to find and initialize any dictionary providers.";
			if (this.failIfNoneConfigured)
			{
			    RuntimeException ex = new RuntimeException(baseMsg);
			    LOG.error("initialization failure", ex);
			    throw ex;
			}
			else
			{
			    LOG.error(baseMsg + " This instance will be useless.");
			}
		}
		
		return new DictionaryProviderSource(initialized);
	}
	
	/**
	 * Initialize the given provider.  If successful, returns a reference to 
	 * the initialized provider, returns <code>null</code> otherwise.
	 * 
	 * @param provider provider to init
	 * @param dbConfig database config (to supply to providers that require DB
	 * access)
	 * @return provider, or <code>null</code> if initialization failed
	 */
	private final DictionaryProvider
	initializeProvider(DictionaryProvider provider, DBConfig dbConfig)
	{
	    if (provider instanceof ConfigurableProvider)
	    {
	        ConfigurableProvider cp = ((ConfigurableProvider) provider);
	        Properties config = 
	                this.forProvider(cp.configurationIdentifier());
	        
	        cp.setProperties(config);
	    }
	    
		if (provider instanceof DBDictionaryProvider)
		{
			try
			{
				final DBDictionaryProvider dbp = ((DBDictionaryProvider) provider);
				dbConfig.initializeProvider(dbp);
			}
			catch (Throwable e)
			{
				LOG.error(String.format(
						"Failed to initialize DB-backed provider, class '%s', skipping. ",
						provider.getClass().getCanonicalName()),
						e);
				return null;
			}
		}
		else if (provider instanceof SimpleDictionaryProvider)
		{
			try
			{
				((SimpleDictionaryProvider) provider).initialize();
			}
			catch (Throwable e)
			{
				LOG.error(String.format(
						"Failed to initialize provider, class '%s', skipping. ",
						provider.getClass().getCanonicalName()),
						e);
				return null;
			}
		}
		
		return provider;
	}
	
	private URLClassLoader
	createLoader() throws IOException
	{
		final List<URL> urls = new ArrayList<>();
		final List<File> searchPaths = 
				this.additionalPaths
						.stream()
						.map(p -> new File(p))
						.collect(Collectors.toList());
		
		LOG.debug("Creating loader for search paths: " + 
					searchPaths.stream()
								.map(f -> "'" + f.getAbsolutePath() + "'")
								.collect(Collectors.joining(", ")));
		
		for (final File f : searchPaths)
		{
			if (! f.canRead())
			{
				LOG.error(String.format(
						"Unable to read search path '%s', skipping", 
						f.getAbsolutePath()));
				continue;
			}
			
			try
			{
			    URL url = f.toURI().toURL();
			    
			    if (f.isDirectory())
			    {
			        LOG.info("Will scan dir: {}", f.getCanonicalPath());
			        url = new URL(url.toString());
			    }
				urls.add(url);
			}
			catch (MalformedURLException e)
			{
				LOG.error(String.format("Skipping malformed path '%s'",
							f.toURI().toString()));
			}
		}
		
		if (urls.isEmpty())
		{
		    return null;
		}
		
		return new URLClassLoader(urls.toArray(new URL[0]), 
									this.getClass().getClassLoader()); 
	}
	
	/**
     * Get the configuration properties configured for a provider with the given
     * identifier.
     * 
     * @param identifier identifier, not <code>null</code>, not blank
     * @return properties, not <code>null</code>
     */
    @NotNull
    public Properties
    forProvider(String identifier)
    {
        if (! this.providerProperties.containsKey(identifier))
        {
            return new Properties();
        }
        return new Properties(this.providerProperties.get(identifier));
    }
	
	@Override
    public void afterPropertiesSet() 
    throws Exception
    {
        if (this.providerConfigs == null)
        {
            return;
        }
        
        for (Entry<String, String> e : this.providerConfigs.entrySet())
        {
            String[] split = StringUtils.split(e.getKey(), '.');
            if (split.length < 2)
            {
                continue;
            }
            String id = split[0];
            String subKey = e.getKey().substring((id + ".").length());
            
            Properties p = this.providerProperties.get(id);
            if (p == null)
            {
                p = new Properties();
                this.providerProperties.put(id, p);
            }
            p.put(subKey, e.getValue());
        }
        
    }

}
