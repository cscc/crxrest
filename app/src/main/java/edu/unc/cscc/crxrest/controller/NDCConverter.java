package edu.unc.cscc.crxrest.controller;

import org.springframework.boot.context.properties.ConfigurationPropertiesBinding;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import edu.unc.cscc.crxrest.model.NDC;

/**
 * A Spring {@link Converter converter} that delegates to 
 * {@link NDC#parse(String) NDC for parsing}
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
@Component
@ConfigurationPropertiesBinding
public class NDCConverter 
implements Converter<String, NDC>
{

	@Override
	public NDC 
	convert(String str)
	{
		return NDC.parse(str);
	}

}
