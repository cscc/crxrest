package edu.unc.cscc.crxrest.config;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

import javax.sql.DataSource;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.stereotype.Component;

import edu.unc.cscc.crxrest.DBDictionaryProvider;
import edu.unc.cscc.crxrest.DBDictionaryProvider.DBType;

/**
 * Configuration/management of database-related things.
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
@Component
public class DBConfig
{

	private static final Logger		LOGGER = LoggerFactory.getLogger("db-config");

	private final int				poolSize;
	private final Random			random;
	private final File				dbDir;
	
	@Autowired
	public DBConfig(@Value("${crxrest.database.pool-size}") int poolSize,
					@Value("${crxrest.database.path}") String dbPath)
	{
		this.poolSize = poolSize;
		this.random = new Random();
		this.dbDir = new File(dbPath);
	}
	
	public final void initializeProvider(DBDictionaryProvider provider)
	throws Throwable
	{
		final DBType type = provider.requiredType();

		final String pid = provider.persistenceID();
		final File path = this.getDBPath(pid);

		String jdbcURL = null;
		boolean init = false;

		/* we'll do persistence iff the source offers us an ID *AND* if we have
		 * a storage directory to write to
		 */
		if (path != null)
		{
			if (pid.isEmpty() || StringUtils.isWhitespace(pid))
			{
				throw new RuntimeException(
					String.format("Provider (class: '%s') produced garbage " 
									+ "persistence ID, refusing to initialize.",
									provider.getClass().getCanonicalName()));
			}

			LOGGER.debug(String.format("Checking if DB path '%s' exists", 
										path.getCanonicalPath()));

			init = ! path.exists();

			/* if path doesn't exist, check that we can create it */
			if (! path.exists())
			{
				if (! path.getParentFile().canWrite())
				{
					throw new IOException(String.format(
							"Unable to write to DB path '%s' for new DB", 
							path.getCanonicalPath()));
				}
			}
			
			if (DBType.SQLITE.equals(type))
			{
				jdbcURL = "jdbc:sqlite:file:" + path + "?cache=shared";
			}
			else if (DBType.HSQL.equals(type))
			{
				jdbcURL = "jdbc:hsqldb:file:" + path;
			}
			else
			{
				throw new IllegalArgumentException("Unknown DB type " + type.name());
			}
		}
		else
		{
			if (DBType.SQLITE.equals(type))
			{
				jdbcURL = "jdbc:sqlite:file:" 
						+ System.currentTimeMillis() + "." 
						+ this.random.nextInt(Integer.MAX_VALUE)
						+ "?mode=memory&cache=shared";
			}
			else if (DBType.HSQL.equals(type))
			{
				jdbcURL = "jdbc:hsqldb:mem:" 
						+ System.currentTimeMillis() + "." 
						+ this.random.nextInt(Integer.MAX_VALUE);
			}
			else
			{
				throw new IllegalArgumentException("Unknown DB type " + type.name());
			}
			init = true;
		}		

		final DataSource ds = this.createDataSource(jdbcURL);
		if (init)
		{
			ResourceDatabasePopulator populator = 
					new ResourceDatabasePopulator();
			
			InputStream schemaStream = provider.schemaStream();
			
			populator.addScript(new InputStreamResource(schemaStream));
			
			DatabasePopulatorUtils.execute(populator, ds);
			
			schemaStream.close();
		}
		
		/* initialize, marking for deletion any DB file on failure
		 * 
		 * FIXME: technically this is a possible res leak.
		 */

		try
		{
			provider.initialize(ds, init);
		} 
		catch (Throwable e)
		{
			if (path != null)
			{
				path.delete();
			}
			throw e;
		}
	}
	
	private final DataSource createDataSource(final String jdbcURL)
	{
		final HikariConfig config = new HikariConfig();
		
		config.setMaximumPoolSize(this.poolSize);
		config.setMaxLifetime(0);
		config.setIdleTimeout(0);
		
		config.setJdbcUrl(jdbcURL);
		
		return new HikariDataSource(config);
	}

	private final File getDBPath(String dbID)
	throws IOException
	{
		if (dbID == null || this.dbDir == null )
		{
			return null;
		}
		
		if (!  this.dbDir.canWrite())
		{
			throw new IOException(String.format("Unable to write to DB path '%s'", 
									this.dbDir.getCanonicalPath()));
		}

		return new File(this.dbDir.getCanonicalPath() 
								+ File.separator 
								+ dbID + ".crxrest.db");
	}
		
}
