ndc-dictionary
===

`ndc-dictionary` is a dictionary provider module designed to provide drug 
information taken from the NDC database (freely available from the FDA at 
[this location](https://www.fda.gov/Drugs/InformationOnDrugs/ucm142438.htm))

It is intended to serve as both a useful source of drug information and an
example implementation of a drug dictionary provider for CRxREST.

Building
---

`ndc-dictionary` will be built as part of the `crxrest` project's build
cycle.  The NDC data files are **not** required to build the project, as they
are only used at run time.

Usage
---

This dictionary provider requires the FDA's NDC data files, specifically the 
`product.txt` file.  On startup the provider will attempt to load the file
from the path specified by the system property  `crxrest.providers.ndc_dictionary.product_path`.  If this property is 
not specified, the provider will attempt to load the file from 
`/usr/local/share/crxrest/ndc/product.txt`

Note on Substances (Drugs)
---

Some entries in the NDC dataset do not list ingredients (i.e. "substances", 
as found in the `SUBSTANCENAME` column.

In these cases, the non-proprietary name will be used instead when modeling
the product's components (see below.)  If no non-proprietary name is available
(rare, but possible), the proprietary name will be used.

All Drug names will be normalized to uppercase.

Notes on Products/Doses
---

Due to the way that the NDC dataset is provided, dosage information is
modeled within this dictionary provider in the following manner for products
consisting of a single substance:

1) UnitOfMeasure is created from the `DOSAGEFORMNAME` field for each entry 
in `product.txt`.

2) For each Product created from an entry in `product.txt` the Dose is 
specified as being a single unit of the UnitOfMeasure defined in #1

For products consisting of more than one substance the product consists of 
multiple ingredients, each with a Dose consisting of 0 units of a 
UnitOfMeasure of the UNDEFINED type (see: Dose.UNDEFINED).

This is done because active ingredient quantities are not represented (or
represented incompletely) within the underlying dataset.  Without using a
third party data set, there is no better way to model this information.

Update: For multi-component drugs, dose/unit is assigned based on the 
order in which the components appear in the data file (product.txt).