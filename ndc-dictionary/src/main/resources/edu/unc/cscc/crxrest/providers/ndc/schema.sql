DROP TABLE IF EXISTS "non_proprietary_names";
DROP TABLE IF EXISTS "substances";
DROP TABLE IF EXISTS "entries";

CREATE TABLE "entries" (
	"id"						UUID PRIMARY KEY NOT NULL,
	"product_id"				VARCHAR(256) NOT NULL,
	"ndc"						VARCHAR(14) NOT NULL,
	"dosage_form"				VARCHAR(256) NOT NULL,
	"active_numerator_strength"	VARCHAR(256),
	"active_ingred_unit"		VARCHAR(256),
	"proprietary_name"			VARCHAR(512) NOT NULL,
	"phonetic_hash"				VARCHAR(32) NOT NULL
);
CREATE INDEX IF NOT EXISTS "entries_n_idx" ON "entries" ("ndc");
CREATE INDEX IF NOT EXISTS "entries_d_idx" ON "entries" ("dosage_form");
CREATE INDEX IF NOT EXISTS "entries_pn_idx" ON "entries" ("proprietary_name");
CREATE INDEX IF NOT EXISTS "entries_pn_hash" ON "entries" ("phonetic_hash");

CREATE TABLE "non_proprietary_names" (
	"id__entries"		UUID NOT NULL,
	"name"				VARCHAR(512) NOT NULL,
	"phonetic_hash"		VARCHAR(32) NOT NULL,

	FOREIGN KEY ("id__entries") REFERENCES "entries" ("id")
);
CREATE INDEX IF NOT EXISTS "npn_id" ON "non_proprietary_names" ("id__entries");
CREATE INDEX IF NOT EXISTS "npn_name" ON "non_proprietary_names" ("name");
CREATE INDEX IF NOT EXISTS "npn_pn_hash" ON "non_proprietary_names" ("phonetic_hash");

CREATE TABLE "substances" (
	"id__entries"		UUID NOT NULL,
	"name"				VARCHAR(512) NOT NULL,
	"phonetic_hash"		VARCHAR(32) NOT NULL,

	FOREIGN KEY ("id__entries") REFERENCES "entries" ("id")
);
CREATE INDEX IF NOT EXISTS "substance_entry_id_fkey_idx" ON "substances" ("id__entries");
CREATE INDEX IF NOT EXISTS "substance_name_idx" ON "substances" ("name");
CREATE INDEX IF NOT EXISTS "substance_pn_hash" ON "substances" ("phonetic_hash");

