/*-
 * ========================LICENSE_START=================================
 * ndc-dictionary
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.crxrest.providers.ndc;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.sql.DataSource;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.Table;
import org.jooq.conf.RenderNameStyle;
import org.jooq.conf.Settings;
import org.jooq.impl.DSL;
import org.jooq.impl.DefaultDSLContext;

import com.fasterxml.uuid.NoArgGenerator;
import com.fasterxml.uuid.impl.RandomBasedGenerator;

import edu.unc.cscc.crxrest.ConfigurableProvider;
import edu.unc.cscc.crxrest.DBDictionaryProvider;
import edu.unc.cscc.crxrest.DictionaryService;
import edu.unc.cscc.crxrest.Discoverable;

@Discoverable
public class NDCServiceProvider
implements DBDictionaryProvider, ConfigurableProvider
{
	public static final String             IDENTIFIER = "FDA-NDC";
	public static final String             DEFAULT_PATH = 
			"/usr/local/share/crxrest/ndc/product.txt";
	
	static final Table<?>                  D_TABLE = 
			DSL.table(DSL.name("entries"));
	static final Table<?>                  N_TABLE = 
			DSL.table(DSL.name("non_proprietary_names"));
	static final Table<?>                  S_TABLE = 
			DSL.table(DSL.name("substances"));
	
	private final NoArgGenerator           uuidGenerator = 
	        new RandomBasedGenerator(null);
	
	private String                         productFilePath;
	
	private DictionaryService              ndcService;
	
	@Override
	public String persistenceID()
	{
		return "DB-" + this.getClass().getCanonicalName();
	}
	
	@Override
	public void 
	initialize(DataSource ds, boolean newDS)
	throws IOException
	{
		/* set up our database */
		final DSLContext ctx = this.initializeDB(ds);
		
		if (! newDS)
		{
			/* set up service */
			this.ndcService = new NDCDictionaryService(ctx, 
											new NDCDictionary(IDENTIFIER, "persistent"));
			return;
		}

		/* see if we have a search path defined */
		
		if (this.productFilePath == null)
		{
			throw new IOException("No dictionary file path specified");
		}
		
		final File f = new File(this.productFilePath);
		
		if (! f.canRead())
		{
			throw new IOException(String.format(
					"Unable to read dictionary file at '%s'", 
					f.getCanonicalPath()));
		}
		
		/* set up our parser */
		ProductParser parser = ProductParser.create(new FileInputStream(f));	
		
		ctx.transaction(config -> {
			for (final ProductParser.Entry e : parser)
			{
				
				final UUID id = this.uuidGenerator.generate();
				
				DSL.using(config)
					.insertInto(D_TABLE)
					.set(field(name("id"), UUID.class), id)
					.set(field(name("product_id"), String.class), 
							e.getProductID())
					.set(field(name("ndc"), String.class), 
							e.getProductNDC())
					.set(field(name("dosage_form"), String.class), 
							e.getDosageFormName())
					.set(field(name("active_numerator_strength"), String.class), 
							e.getActiveNumeratorStrength())
					.set(field(name("active_ingred_unit"), String.class), 
							e.getActiveIngredUnit())					
					.set(field(name("proprietary_name"), String.class), 
							e.getProprietaryName())
					.set(field(name("phonetic_hash"), String.class), 
							PhoneticHash.hash(e.getProprietaryName()))
					.execute();
				
				DSL.using(config)
					.batch(e.getNonProprietaryNames()
							.stream()
							.map(name -> 
									DSL.using(config).insertInto(N_TABLE)
										.set(field(name("id__entries"), UUID.class), id)
										.set(field(name("name"), String.class), name)
										.set(field(name("phonetic_hash"), String.class),
											PhoneticHash.hash(name)))
							.collect(Collectors.toList()))
					.execute();
				
				DSL.using(config)
					.batch(e.getSubstanceNames()
							.stream()
							.map(name -> 
									DSL.using(config).insertInto(S_TABLE)
										.set(field(name("id__entries"), UUID.class), id)
										.set(field(name("name"), String.class), name)
										.set(field(name("phonetic_hash"), String.class),
											PhoneticHash.hash(name)))
							.collect(Collectors.toList()))
					.execute();
			}
		});
		
		parser.close();
		
		/* set up service */
		this.ndcService = new NDCDictionaryService(ctx, 
										new NDCDictionary(IDENTIFIER, productFilePath));
	}

	@Override
	public Set<String>
	providedDictionaries()
	{
		return Collections.singleton(IDENTIFIER);
	}

	@Override
	public DictionaryService 
	load(String identifier)
	{
		if (this.ndcService == null)
		{
			throw new RuntimeException(
					"Cannot provide service; provider was not inititalized.  "
					+ "This is a bug in the calling code (CRxRest application).");
		}
		
		if (IDENTIFIER.equals(identifier))
		{
			return this.ndcService;
		}
		
		return null;
	}
	
	private DSLContext
	initializeDB(DataSource ds)
	{
		Settings settings = new Settings();
		SQLDialect dialect = SQLDialect.SQLITE;
		settings.withRenderNameStyle(RenderNameStyle.QUOTED);
		
		DSLContext ctx = new DefaultDSLContext(ds, dialect, settings);
		
		return ctx;
	}

	@Override
	public InputStream
	schemaStream() 
	throws IOException
	{
		return this.getClass()
					.getClassLoader()
					.getResourceAsStream("edu/unc/cscc/crxrest/providers/ndc/schema.sql");
	}
	
	@Override
	public @NotNull @NotBlank String 
	configurationIdentifier()
	{
	    return "crx-ndc";
	}

    @Override
    public void 
    setProperties(Properties properties)
    {
        this.productFilePath = properties.getProperty("product-path");
        
        if (this.productFilePath == null)
        {
            this.productFilePath = DEFAULT_PATH;
        }
    }
}
