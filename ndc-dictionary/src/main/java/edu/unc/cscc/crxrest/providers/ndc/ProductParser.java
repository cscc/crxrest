/*-
 * ========================LICENSE_START=================================
 * ndc-dictionary
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.crxrest.providers.ndc;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Stream;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;

import edu.unc.cscc.crxrest.providers.ndc.ProductParser.Entry;

/**
 * Parser for NDC data files.
 * 
 * Format documentation available from 
 * <a href="https://www.fda.gov/Drugs/InformationOnDrugs/default.htm">FDA.gov</a>
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
public final class ProductParser
implements Iterable<Entry>, Closeable
{
	private static final String[]	colNames = {
			"PRODUCTID", "PRODUCTNDC", "PRODUCTTYPENAME", "PROPRIETARYNAME", 
			"PROPRIETARYNAMESUFFIX", "NONPROPRIETARYNAME", "DOSAGEFORMNAME", 
			"ROUTENAME", "STARTMARKETINGDATE", "ENDMARKETINGDATE", 
			"MARKETINGCATEGORYNAME", "APPLICATIONNUMBER", "LABELERNAME", 
			"SUBSTANCENAME", "ACTIVE_NUMERATOR_STRENGTH", 
			"ACTIVE_INGRED_UNIT", "PHARM_CLASSES", "DEASCHEDULE"
	};
	
	private final InputStream		in;
	private final BufferedReader	reader;
	
	private CSVParser				parser;
	private Iterator<CSVRecord>		lineIter;
	
	private ProductParser(InputStream in, Charset cs)
	{
		this.in = in;
		this.reader = new BufferedReader(new InputStreamReader(in, cs));
	}
	
	/**
	 * Read and parse the next entry from the backing input.
	 * 
	 * @return next entry or <code>null</code> if the input has been exhausted
	 * @throws IOException thrown if an error occurred while reading the next
	 * line of the input
	 * @throws IllegalArgumentException thrown if the read line could not be 
	 * parsed
	 */
	public Entry
	next()
	throws IOException
	{
		CSVRecord r;
		
		try
		{
			r = this.lineIter.next();
		}
		catch (NoSuchElementException e)
		{
			return null;
		}
		
		return new Entry(r.get("PRODUCTID"), 
							r.get("PRODUCTNDC"), 
							r.get("PRODUCTTYPENAME"), 
							r.get("PROPRIETARYNAME"), 
							r.get("DOSAGEFORMNAME"),
							r.get("ACTIVE_NUMERATOR_STRENGTH"),
							r.get("ACTIVE_INGRED_UNIT"),
							parseMVColumn(r.get("NONPROPRIETARYNAME")), 
							parseMVColumn(r.get("SUBSTANCENAME")));
		
	}
	
	/**
	 * Whether the underlying input has any lines which have not been parsed.
	 * 
	 * @return <code>true</code> if the input has lines which have not been 
	 * parsed, <code>false</code> otherwise
	 */
	public boolean
	hasNext()
	{
		return this.lineIter.hasNext();
	}
	
	/**
	 * Initialize the parser.
	 * 
	 * @throws IOException thrown if initialization failed
	 */
	public void
	init()
	throws IOException
	{
		if (this.parser == null)
		{
			this.parser =  CSVFormat.TDF.withHeader().parse(this.reader);
			this.lineIter = this.parser.iterator();
			
			/* check that the header contains sane things */
			final Map<String, Integer> headerMap = this.parser.getHeaderMap();
			final boolean valid = 
					Stream.of(colNames).allMatch(headerMap :: containsKey);
			
			if (! valid)
			{
				throw new IOException("Unrecognized header");	
			}
		}
	}
	
	
	@Override
	public Iterator<Entry>
	iterator()
	{
		return new Iterator<Entry>()
		{

			@Override
			public boolean
			hasNext()
			{
				return ProductParser.this.hasNext();
			}

			@Override
			public Entry next()
			{
				try
				{
					return ProductParser.this.next();
				}
				catch (IOException e)
				{
					throw new RuntimeException(e);
				}
			}
			
		};
	}
	
	/**
	 * Release the resources held by the parser.  This will close any 
	 * input streams provided to the parser at the time of creation, as well
	 * as any readers or other resources used internally.
	 * 
	 * @throws IOException 
	 */
	@Override
	public void
	close()
	throws IOException
	{
		if (this.parser != null)
		{
			this.parser.close();
		}
		this.reader.close();
		this.in.close();
	}

	/**
	 * Create a parser which will consume the given input stream.
	 * 
	 * @param in stream to consume
	 * @return parser
	 * @throws IOEXception thrown if the parser could not be initialized
	 */
	public static final ProductParser
	create(InputStream in) throws IOException
	{
		return create(in, Charset.defaultCharset());
	}
	
	/**
	 * Create a parser which will consume the given input stream, interpreting
	 * it according to the rules of the given character set.
	 * 
	 * @param in stream to consume
	 * @param cs character set against which to interpret stream
	 * @return parser
	 * @throws IOException thrown if the parser could not be initialized
	 */
	public static final ProductParser
	create(InputStream in, Charset cs)
	throws IOException
	{
		ProductParser parser;
		
		if (in instanceof BufferedInputStream)
		{
			parser = new ProductParser(in, cs);
		}
		
		parser = new ProductParser(new BufferedInputStream(in, 16 * 1024), cs);
		
		parser.init();
		
		return parser;
	}
	
	private static final List<String>
	parseMVColumn(String col)
	{
		if (StringUtils.isBlank(col))
		{
			return Collections.emptyList();
		}

		final String[] split = col.split("\\Q;\\E");
		final List<String> res = new ArrayList<>(split.length);
		
		for (String s : split)
		{
			s = StringUtils.trimToNull(s);
			if (s != null)
			{
				res.add(s);
			}
		}
		
		return res;
	}
	
	public static final class Entry
	{
		private final String			productID;
		private final String			productNDC;
		private final String			productTypeName;
		private final String			proprietaryName;
		private final String			dosageFormName;
		private final String			activeNumeratorStrength;
		private final String			activeIngredUnit;
		private final List<String>		nonProprietaryNames;
		private final List<String>		substanceNames;
		
		
		
		private Entry(String productID, String productNDC,
				String productTypeName, String proprietaryName,
				String dosageFormName,
				String activeNumeratorStrength, String activeIngredUnit,
				List<String> nonProprietaryNames, List<String> substanceNames)
		{
			this.productID = productID;
			this.productNDC = productNDC;
			this.productTypeName = productTypeName;
			this.proprietaryName = proprietaryName;
			this.dosageFormName = dosageFormName;
			this.activeNumeratorStrength = activeNumeratorStrength;
			this.activeIngredUnit = activeIngredUnit;
			this.nonProprietaryNames = nonProprietaryNames;
			this.substanceNames = substanceNames;
		}

		public final String 
		getProductID()
		{
			return this.productID;
		}

		public final String 
		getProductNDC()
		{
			return this.productNDC;
		}

		public final String 
		getProductTypeName()
		{
			return this.productTypeName;
		}

		public final String 
		getProprietaryName()
		{
			return this.proprietaryName;
		}
		
		public final String getDosageFormName()
		{
			return this.dosageFormName;
		}
		
		public final String getActiveNumeratorStrength()
		{
			return this.activeNumeratorStrength;
		}
		
		public final String getActiveIngredUnit()
		{
			return this.activeIngredUnit;
		}

		public final List<String> 
		getNonProprietaryNames()
		{
			return this.nonProprietaryNames;
		}

		public final List<String> 
		getSubstanceNames()
		{
			return this.substanceNames;
		}
		
	}
}
