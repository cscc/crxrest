/*-
 * ========================LICENSE_START=================================
 * ndc-dictionary
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.crxrest.providers.ndc;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonTypeName;

import edu.unc.cscc.crxrest.model.NDC;
import edu.unc.cscc.crxrest.model.barcode.Barcode;
import edu.unc.cscc.crxrest.model.product.Product;
import edu.unc.cscc.crxrest.model.product.ProductComponent;

/**
 * {@link Product} implementation which corresponds to the NDC dictionary's
 * notion of a "product".
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
@JsonTypeName("ndc-product")
public class NDCProduct
implements Product
{
	private final Set<ProductComponent>		components;
	private final String					dosageForm;
	private final String					activeNumeratorStrength;
	private final String					activeIngredUnit;
	private final String					name;
	private final NDC						ndc;
	private final Set<String>				npns;
	
	public NDCProduct(String name, NDC ndc, Set<String> npns, 
						Set<ProductComponent> components, String dosageForm, String activeNumeratorStrength, String activeIngredUnit)
	{
		this.components = new HashSet<>(components);
		this.dosageForm = dosageForm;
		this.activeNumeratorStrength = activeNumeratorStrength;
		this.activeIngredUnit = activeIngredUnit;
		this.name = name;
		this.ndc = ndc;
		this.npns = new HashSet<>(npns);
	}

	@Override
	public String 
	name()
	{
		return this.name;
	}

	@Override
	public Set<ProductComponent> 
	components()
	{
		return Collections.unmodifiableSet(this.components);
	}

	@Override
	public NDC 
	ndc()
	{
		return this.ndc;
	}

	@Override
	public Set<Barcode<?>> 
	barcodes()
	{
		return Collections.emptySet();
	}
	
	@JsonGetter("non-proprietary-names")
	public Set<String>
	nonProprietaryNames()
	{
		return Collections.unmodifiableSet(this.npns);
	}
	
	@JsonGetter("dosage-form")
	public String
	dosageForm()
	{
		return this.dosageForm;
	}
	
	@JsonGetter("active_numerator_strength")
	public String
	activeNumeratorStrength()
	{
		return this.activeNumeratorStrength;
	}
	
	@JsonGetter("active_ingred_unit")
	public String
	activeIngredUnit()
	{
		return this.activeIngredUnit;
	}	

	@Override
	public String 
	displayName()
	{
		return this.name() 
				+ " (NDC: " + this.ndc().toString() 
				+ componentStrength()
				+ ", [dosage form: " + this.dosageForm() + "])";
	}
	
	public String componentStrength() 
	{
		StringBuffer dispName = new StringBuffer();
		dispName.append(", [Components: ");
		for (ProductComponent p : this.components) {
			dispName.append(p.drug().canonicalName() + " " + p.dose().name() + " " + p.dose().unit().name() + ", ");
		}
		dispName.setLength(dispName.length() - 2);
		dispName.append("]");
		
		return dispName.toString();
	}

}
