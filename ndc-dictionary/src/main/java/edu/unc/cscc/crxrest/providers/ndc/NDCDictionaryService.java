/*-
 * ========================LICENSE_START=================================
 * ndc-dictionary
 * %%
 * Copyright (C) 2017 CSCC - University of North Carolina
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the CSCC - University of North Carolina nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * =========================LICENSE_END==================================
 */
package edu.unc.cscc.crxrest.providers.ndc;

import static edu.unc.cscc.crxrest.providers.ndc.NDCServiceProvider.D_TABLE;
import static edu.unc.cscc.crxrest.providers.ndc.NDCServiceProvider.N_TABLE;
import static edu.unc.cscc.crxrest.providers.ndc.NDCServiceProvider.S_TABLE;
import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.Stream.Builder;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.impl.DSL;

import edu.unc.cscc.crxrest.Dictionary;
import edu.unc.cscc.crxrest.DictionaryService;
import edu.unc.cscc.crxrest.model.Dose;
import edu.unc.cscc.crxrest.model.Drug;
import edu.unc.cscc.crxrest.model.NDC;
import edu.unc.cscc.crxrest.model.TradeName;
import edu.unc.cscc.crxrest.model.UnitOfMeasure;
import edu.unc.cscc.crxrest.model.barcode.Barcode;
import edu.unc.cscc.crxrest.model.product.Product;
import edu.unc.cscc.crxrest.model.product.ProductComponent;


public class NDCDictionaryService
implements DictionaryService
{
	public static final int			MIN_QUERY_LEN = 2;

	private static final int		RESULT_LIMIT = 1000;
	
	/**
	 * Distance that we will fuzz hashes.
	 */
	private static final int		HASH_DISTANCE = 3;
	/**
	 * Minimum length of hashes generated as part of our fuzzing process.
	 */
	private static final int		MIN_FUZZY_HASH_LEN = 3;
			
	private final DSLContext		ctx;
	private final Dictionary		dictionary;
	
	NDCDictionaryService(DSLContext ctx, Dictionary dictionary)
	{
		this.ctx = ctx;
		this.dictionary = dictionary;
	}

	@Override
	public int
	resultLimit()
	{
		return RESULT_LIMIT;
	}

	@Override
	public Dictionary
	dictionary()
	{
		return this.dictionary;
	}

	@Override
	public List<TradeName> 
	findTradeNames(final String query, int limit)
	{
		limit = limit > 0 ? limit : this.resultLimit();

		if (query.length() < MIN_QUERY_LEN)
		{
			return Collections.emptyList();
		}
		
		/* weirdness warning: we limit twice.  Once in the query, then one
		 * again in the result processing.  This is because trade names -> drug
		 * names are many-to-many, and thus we can have > n TradeName results
		 * even if we only got n proprietary names.
		 */
		
		final Field<String> f = field(name("proprietary_name"), String.class); 
		
		/* load the trade names for our drugs, key is product ID */
		Map<UUID, String> names = 
				this.ctx.select(field(name("id"), UUID.class), f)
						.from(D_TABLE)
						.where(f.containsIgnoreCase(query))
						.orderBy(f.asc())
						.limit(limit)
						.fetchMap(field(name("id"), UUID.class), f);
						
		/* figure out the NPNs for the IDs in our trade name map */
		Map<UUID, List<String>> npns = 
				this.ctx.selectFrom(N_TABLE)
						.where(field(name("id__entries"), UUID.class)
										.in(names.keySet()))
						.fetchGroups(field(name("id__entries"), UUID.class), 
										field(name("name"), String.class));
		
		/* combine, and return */
		
		List<TradeName> tradeNames = new ArrayList<>();
		
		names.forEach((id, name) -> {
			List<String> drugNames = npns.get(id);
			for (String npn : drugNames)
			{
				tradeNames.add(new TradeName(name, new Drug(npn)));
			}
		});
		
		if (tradeNames.size() > limit)
		{
			return tradeNames.subList(0, limit);
		}
				
		return tradeNames;
	}

	@Override
	public List<Drug> 
	findDrugs(final String query, int limit)
	{
		limit = limit > 0 ? limit : this.resultLimit();
		
		if (query.length() < MIN_QUERY_LEN)
		{
			return Collections.emptyList();
		}

		final Field<String> nf = field(name("name"), String.class);
		
		final Set<Drug> drugs = this.ctx
								.select(nf)
								.from(N_TABLE)
								.where(nf.containsIgnoreCase(query))
								.fetch(r -> new Drug(r.get(nf).toUpperCase()))
								.stream()
								.collect(Collectors.toCollection(HashSet :: new));

		/* fuzzy hash search */

		final List<List<String>> hashes = 
			partitionHashesByLength(hashFuzz(query, HASH_DISTANCE, MIN_FUZZY_HASH_LEN));
		
		final Field<String> hashField =
			field(name("phonetic_hash"), String.class);

		for (final List<String> hashGroup : hashes)
		{
			if (drugs.size() >= limit)
			{
				break;
			}

			Condition cond = DSL.falseCondition();

			for (String hash : hashGroup)
			{
				cond = cond.or(hashField.like(hash));
			}

			this.ctx
				.select(nf)
				.from(N_TABLE)
				.where(cond)
				.fetch(nf)
				.stream()
				.map(n -> new Drug(n.toUpperCase()))
				.forEach(drugs :: add);
		}

		// Add strength, units and dosage-form to Drugs
		Set<Drug> drugsWithDosageInfo = new HashSet<>();
		
		for (Drug d : drugs)
		{			
			// get strength, units and dosage-form for drugs based on non-proprietary names (TODO: change this to JOOQ later)
			// String sql = "select npn.name, e.id, e.active_numerator_strength, e.active_ingred_unit, e.dosage_form from non_proprietary_names npn, entries e where e.id = npn.id__entries and upper(npn.name) ='" + d.canonicalName() + "'";
			String sql = "select npn.name, e.id, e.active_numerator_strength, e.active_ingred_unit, e.dosage_form "
						+ " from non_proprietary_names npn, entries e "
						+ " where e.id = npn.id__entries " 
						+ " and upper(npn.name) ='" + d.canonicalName() + "'";
			
			// execute query and get 
			Result<Record> result = this.ctx.fetch(sql);			
			
			for (Record r : result) {
			    //String name = (String) r.get("name");
			    //UUID id = (UUID) r.get("id");
				
			    String active_numerator_strength = (String) r.get("active_numerator_strength");
			    String active_ingred_unit = (String) r.get("active_ingred_unit");
			    String dosage_form = (String) r.get("dosage_form");
			    
			    drugsWithDosageInfo.add(new Drug(d.canonicalName(), d.chemicalFormula(), d.synonyms(), active_numerator_strength, active_ingred_unit, dosage_form));
			}	
		}
		System.out.println();

		/*return drugs.stream()
					.sorted((a, b) -> {
						double score = 
							biScore(query.toUpperCase(), b.canonicalName().toUpperCase())
								-
								biScore(query.toUpperCase(), a.canonicalName().toUpperCase());
						
						return (int) Math.round(score * 1000);
					})
					.collect(Collectors.toList());*/
		
		return drugsWithDosageInfo.stream()
				.sorted((a, b) -> {
					double score = 
						biScore(query.toUpperCase(), b.canonicalName().toUpperCase() + b.displayName().toUpperCase())
							-
							biScore(query.toUpperCase(), a.canonicalName().toUpperCase() + a.displayName().toUpperCase());
//					System.out.println("Score " + query.toUpperCase() + "-" + b.canonicalName().toUpperCase() + b.displayName().toUpperCase()
//							+  " another --> " + query.toUpperCase() + "-" + a.canonicalName().toUpperCase() + a.displayName().toUpperCase()
//							+ " is --> " + (int) Math.round(score * 1000));
					return (int) Math.round(score * 1000);
				})
				.collect(Collectors.toList());
	}

	@Override
	public List<Product> 
	findProducts(final String query, int limit)
	{

		/* FIXME - "valsartin" produces poor result ordering
		 *
		 * This search approach has one big hole:
		 * 
		 * "valsartin" produces a top result of 
		 * "Valsartan and hydrochlorothiazide" and not "Valsartan" as it 
		 * should.  Without debugging this, my guess is that both hits are 
		 * scored the same because we're looking at proprietary name, but 
		 * ignoring the fact that a multi-ingredient product should be ranked
		 * lower than a single-ingredient product in cases where we have only 
		 * one input word.
		 */

		if (StringUtils.isEmpty(query) || limit < 1)
        {
            return Collections.emptyList();
        }
        
        if (query.length() < MIN_QUERY_LEN)
		{
			return Collections.emptyList();
        }

        final String term = query.trim();

        final Field<UUID> idField = field(name("id"), UUID.class);

		
        final Function<NDCProduct, Pair<NDCProduct, Double>> scoreMapper = 
            product -> Pair.of(product, scoreProduct(product, term));
        final Comparator<Pair<?, Double>> pairSort = 
            (a, b) -> Double.compare(b.getRight(), a.getRight());
        
        /* Establish a fallback limit.  Since trivially-short strings are likely
         * to produce huge numbers of mostly-irrelevant results, we limit our
         * queries to fewer results.  This prevents us from burning a ton of 
         * time finding everything with, say "a" in the product name, while 
         * still allowing us to have plenty of results for longer queries 
         * (where matches are likely to be more significant).
         */
        final int fbLimit = term.length() * 250;


        final List<String> termWords = 
                    Arrays.asList(StringUtils.split(term.toUpperCase()));


        /* generate a stream of our search conditions (for latter mapping to stages) */
        Builder<Condition> builder = Stream.builder();

        /* exact term search */
        builder.accept(field(name("proprietary_name"), String.class).containsIgnoreCase(term));
        /* exact phonetic search */
        builder.accept(field(name("phonetic_hash"), String.class)
                        .containsIgnoreCase(PhoneticHash.hash(term)));

        /* pos-independent word search */
        builder.accept(termWords.stream()
                            .map(t -> field(name("proprietary_name")).containsIgnoreCase(t))
                            .reduce(DSL.trueCondition(), (a, b) -> a.and(b)));
        /* all word phonetic search */
        builder.accept(termWords
                        .stream()
                        .map(PhoneticHash :: hash)
                        .map(t -> field(name("phonetic_hash")).containsIgnoreCase(t))
                        .reduce(DSL.trueCondition(), (a, b) -> a.and(b)));
        
        return builder
                    .build()
                    .parallel()
                    .flatMap(c -> 
                        this.loadProducts(
                                this.ctx.select(idField)
                                        .from(D_TABLE)
                                        .where(c)
                                        .limit(fbLimit)
                                        .fetch(idField))
                                .stream()
                                .map(scoreMapper)
                                .sorted(pairSort)
                                .limit(limit)
                    )
                    .unordered()
                    .distinct()
                    .sorted(pairSort)
                    .map(Pair :: getLeft)
                    .limit(limit)
                    .collect(Collectors.toList());
	}

	@Override
	public List<Product> 
	findByNDC(final NDC ndc, int limit)
	{
		limit = limit > 0 ? limit : this.resultLimit();
		
		final String ndcStr = 
			(ndc.complete()
				? (new NDC(ndc.labelerCode(), ndc.productCode()))
				: ndc)
			.toString();
		
		final List<UUID> ids =
				this.ctx.select(field(name("id"), UUID.class))
						.from(D_TABLE)
						.where(field(name("ndc"), String.class).eq(ndcStr))
						.orderBy(field(name("proprietary_name"), String.class).desc())
						.limit(limit)
						.fetch(field(name("id"), UUID.class));
		
		return new ArrayList<Product>(this.loadProducts(ids));
	}

	@Override
	public List<Product> 
	findByBarcode(Barcode<?> barcode, int limit)
	{
		return Collections.emptyList();
	}
	
	private final List<NDCProduct>
	loadProducts(final List<UUID> ids)
	{
		if (ids.isEmpty())
		{
			return Collections.emptyList();
		}
		
		/* why yes, we could do joins here.  We're also running solely in
		 * memory, with no writes, so why bother?
		 */
		
		/* ingredient drugs by product ID */
		Map<UUID, List<Drug>> drugs = 
				this.ctx.selectFrom(S_TABLE)
						.where(field(name("id__entries"), UUID.class).in(ids))
						.fetchGroups(field(name("id__entries"), UUID.class),
									r -> new Drug(r.get(field(name("name"), 
														String.class))));
		
		Map<UUID, List<String>> npNames = 
				this.ctx.selectFrom(N_TABLE)
						.where(field(name("id__entries"), UUID.class).in(ids))
						.fetchGroups(field(name("id__entries"), UUID.class), 
								field(name("name"), String.class));
		
		return this.ctx.selectFrom(D_TABLE)
						.where(field(name("id"), UUID.class).in(ids))
						.fetch(r -> {
							
							final UUID id = r.get(field(name("id"), UUID.class));
							final NDC ndc = 
									NDC.parse(r.get(field(name("ndc"), String.class)));
							
							final Set<String> npns = 
									npNames.containsKey(id)
											? new HashSet<>(npNames.get(id))
											: Collections.emptySet();
											
							final String dosageForm = 
									r.get(field(name("dosage_form"), String.class));
							
							final String activeNumeratorStrength = 
									r.get(field(name("active_numerator_strength"), String.class));
							
							final String activeIngredUnit = 
									r.get(field(name("active_ingred_unit"), String.class));
							
							final String name = 
									r.get(field(name("proprietary_name"), String.class));
														
							List<Drug> d = drugs.get(id);
							
							/* handle products with no listed substances */
							if (d == null)
							{
								String fallback = 
											! npns.isEmpty()
											? npns.stream()
													.collect(Collectors.joining(","))
											: name;
								
								d = Collections.singletonList(new Drug(fallback));
							}
							
							final Dose dose = d.size() >= 1
									? Dose.create(activeNumeratorStrength, new UnitOfMeasure(activeIngredUnit, false), 1, false)
									: Dose.UNDEFINED;
							
							final List<ProductComponent> componentList =   
									d.stream()
										.map(drug -> new ProductComponent(drug, dose))
										.collect(Collectors.toList());
							
							/* 
							 * Handle multi-component drugs with separate dose/unit for each component.
							 * Dose/unit for each component is assigned in the order they appear in the data file.
							 */

							Set<ProductComponent> componentSet = new HashSet<>();
							if (componentList.size() > 1)
							{
								String [] doses = activeNumeratorStrength.split(";");
								String [] units = activeIngredUnit.split(";");
								
								int i=0;
								
								for (ProductComponent p : componentList) {
									if ((i < doses.length) && (i < units.length))
									{
										componentSet.add(new ProductComponent(p.drug(), Dose.create(doses[i].trim(), new UnitOfMeasure(units[i].trim(), false), 1, false)));
										i++;
									} 
									else 
									{
										componentSet.add(new ProductComponent(p.drug(), Dose.UNDEFINED));
										i++;
									}
								}
							} 
							else 
							{
								componentSet.addAll(componentList);
							}
							
							return new NDCProduct(name, ndc, npns, componentSet,
												dosageForm, activeNumeratorStrength, activeIngredUnit);
							
						});
	}

	private static final double
	scoreProduct(NDCProduct p, String query)
	{
		double score = 
			biScore(p.name().toUpperCase(), query.toUpperCase());

		/* NPNs */
		for (String npn : p.nonProprietaryNames())
		{
			double ns = biScore(npn, query);
			if (ns > score)
			{
				score = ns;
			}
		}
		/* component drugs */
		double cs =
			p.components()
				.stream()
				.map(c -> biScore(c.drug().canonicalName().toUpperCase(), query.toUpperCase()))
				.max(Double :: compare)
				.orElse(0.0d);
		
		score = Math.max(score, cs);

		return score;
	}

	private static final double
	biScore(String reference, String variation)
	{
		return score(reference, variation) 
				+ score(PhoneticHash.hash(reference), 
						PhoneticHash.hash(variation));
	}

	private static final Stream<String>
	hashFuzz(final String input, final int distance, final int minHashLen)
	{
		return hashPermute(PhoneticHash.hash(input).toCharArray(), distance, minHashLen)
				.map(String :: valueOf)
				.distinct();
	}
	
	private static final Stream<char[]>
	hashPermute(final char[] hash, final int distance, final int minHashLen)
	{

		if (hash.length < minHashLen)
		{
			return Stream.empty();
		}
		
		Stream.Builder<char[]> builder = Stream.builder();

		builder.accept(hash);

		for (int i = 0; i < hash.length; i++)
		{
			/* do wildcard permutations */
			if (hash[i] != '_')
			{
				char[] w = ArrayUtils.clone(hash);
				w[i] = '_';
				builder.accept(w);
			}
			
			/* do sub-hash permutations if we can */
			if (hash.length > minHashLen && distance > 0)
			{
				hashPermute(ArrayUtils.remove(hash, i), distance - 1, minHashLen)
					.forEach(builder :: accept);
			}
		}

		return builder.build();
	}

	private static final List<List<String>>
	partitionHashesByLength(Stream<String> stream)
	{

		return stream
                .collect(Collectors.toMap(s -> s.length() - StringUtils.countMatches(s, '_'), 
                                            Stream :: of, Stream :: concat))
				.entrySet().stream()
				.sorted((a, b) -> b.getKey() - a.getKey())
				.map(e -> e.getValue().collect(Collectors.toList()))
				.collect(Collectors.toList());

	}

	/**
	 * <p>
	 * Scoring algorithm for comparing two strings.  Uses a word-aware Jaccard
	 * distance implementation.  Words which appear in one input but not the
	 * other are ignored.  The individual words are then scored, and the
	 * scores summed to determine the overall score.  Each scored word will be
	 * scored twice: first based on its Jaccard distance to the word in its
	 * same position in the other input, and second based on the Jaccard
	 * distance between a phonetic has of the word and a phonetic hash of its
	 * complementary word in the other input.
	 * </p>
	 *
	 * <p>If either or both inputs are empty or consist solely of whitespace, 
	 * the resulting score will always be 0.</p>
	 *
	 * @param a first input string, not {@code null}
	 * @param b second input string, not {@code null}
	 *
	 * @return score, with 2n indicating a perfect text and phonetic match
	 * (where n is the number of words scored)
	 */
	private static final double
    score(String a, String b)
    {
		if (StringUtils.isBlank(a) || StringUtils.isBlank(b))
		{
			return 0.0d;
		}

		/* use whitespace-aware shingling + scoring */
		if (StringUtils.containsWhitespace(a) || 
			StringUtils.containsWhitespace(b))
		{
			List<String> wsa = wordShingle(a, 1, null);
			List<String> wsb = wordShingle(b, 1, null);

			List<String> l = (wsa.size() < wsb.size()) ? wsa : wsb;
			List<String> g = (wsa.size() >= wsb.size()) ? wsa : wsb;

			/* compute intersection of l and g, where intersection is defined 
			 * as an n-gram for which the jaccard distance is > 0.0.
			 * 
			 * This implementation is O(nj) where n = len(l) and j = len(g).
			 * 
			 * Yes, this can be improved.  Quite a lot, actually.
			 * 
			 * The inputs are also short enough that we don't care for the 
			 * purposes of experimentation.
			 */

			double score = 0.0d;
			for (String gs : g)
			{
				for (String ls : l)
				{
					score += score(gs, ls);
				}
			}

			return score;
		}

		int shingleSize = Math.min(a.length(), b.length());
		shingleSize = Math.min(shingleSize, 3);

        /* TODO a.length < n-gram size */
        List<String> ang = shingle(a, 3);
        List<String> bng = shingle(b, 3);

        /* jaccard is size of intersection / size of union */
        Set<String> intersection = new HashSet<>(ang);
        intersection.retainAll(bng);

        Set<String> union = new HashSet<>();
        union.addAll(ang);
        union.addAll(bng);

        return ((double) intersection.size() / (double) union.size());
    }

    private static final List<String>
    shingle(String a, int n)
    {
        List<String> ngrams = new ArrayList<>();

        for (int i = 0; i < a.length() - n + 1; i++)
        {
            ngrams.add(a.substring(i, i + n));
        }
            
        return ngrams;
	}
	
	/**
	 * Generate list of n-grams for the given input string, delimiting 
	 * based on word boundaries (whitespace).  The resulting n-grams will 
	 * have their words delimited with the given character sequence.
	 * 
	 * @param a input string
	 * @param n size of n-grams
	 * @param delim delimiting sequence, {@code null} for plain concatenation
	 * @return list of n-grams each of the specified length
	 */
	private static final List<String>
	wordShingle(String a, int n, String delim)
	{
		List<String> ngrams = new ArrayList<>();

		ArrayList<String> split = 
			Arrays.asList(StringUtils.splitByCharacterType(a))
				.stream()
				.filter(StringUtils :: isNotBlank)
				.collect(Collectors.toCollection(ArrayList :: new));

		Collector<CharSequence, ?, String> collector = 
			delim == null ? Collectors.joining() : Collectors.joining(delim);

		for (int i = 0; i < split.size() - n + 1; i++)
		{
			ngrams.add(split.subList(i, i + n)
							.stream()
							.collect(collector));
		}
		
		return ngrams;
	}
}
