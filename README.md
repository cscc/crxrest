# CRxREST

**CRxREST** is a modular, RESTful microservice for the search and coding of 
medications.  It provides a model for representing active ingredients, 
products, and associated metadata.  The provided abstraction is designed to 
accommodate multiple, disparate sources of drug information.

CRxREST provides a RESTful API for searching for trade names, products, etc.
by both free-form query input as well as by NDC and/or barcode information 
(when supported by the underlying data source.)

The dictionaries/datasets against which CRxREST searches are discovered via an
SPI mechanism and are runtime-configurable.

## Concepts

The CRxREST model is thoroughly documented within the 
`edu.unc.cscc.crxrest.model` package.

#### Dictionaries

Drug and product information within CRxREST is obtained from `Dictionary`s.  
A dictionary generally corresponds to an external dataset of some form (for
example: the FDA's NDC database).  As the data represented by various datasets
may vary, dictionaries may declare which portions of the CRxREST model they
support.

The underlying data for a `Dictionary` may come from any source that the 
implementer wishes (e.g. a local file, a remote database.)

#### Dictionary Providers

At the core of the CRxREST module system is a dictionary provider.  A 
dictionary provider is responsible for providing service(s) which may search 
for results within a dictionary. A provider must implement the 
`DictionaryProvider` interface (or one of its sub-interfaces).  In addition,
in order to be eligible for run-time loading, the implementation must be 
annotated with the `@Discoverable` annotation.

### Example Dictionary Provider

The sub-module `ndc-dictionary` serves both a means of supporting the FDA's
NDC dataset and as an example of a dictionary + provider implementation.


## Configuration

All configuration properties are documented within
`src/main/resources/application.yml`.  Parameters may be set at
run-time like so:

`java -jar path/to/app.jar --crxrest.database.pool-size=2`


Dictionary providers may have their own configuration options.  As CRxREST 
does not mandate a particular configuration scheme for providers you should
consult the documentation of the provider(s) in question for information on 
how to configure them.  Providers may elect to support `ConfigurableProvider`,
which will allow their configuration properties to be managed by the CRxREST
instance via the usual Spring-based configuration hierarchy.  See 
`application.yml`for examples.

## API Documentation

OpenAPI documentation will be generated as part of the build process for 
CRxREST.  The resulting documentation will be placed within the `api-docs` 
directory.

Included in the same directory is an HTML file providing the
[ReDoc](https://github.com/Rebilly/ReDoc) viewer.  Unfortunately due to flaw
in the JSON Schema design, the viewer will not be able to load the spec from
the filesystem.  Instead, to view the documentation locally you will have to
launch a web server which will serve the `api-docs` directory.  Yes, really.

## Barcode Search

Currently CRxREST supports UPC-A barcodes.  In an attempt to make this support
more useful, CRxREST is able to use these barcodes to attempt a search against
dictionaries which do not have support for barcodes, but do support NDCs.

UPCs beginning with a `3` (aka UPN) will have their payload extracted, and 
three possible (partial) NDCs created from the resulting data.  These will 
then be used to search the dictionary by NDC for possible matches.

Unfortunately, as it is impossible to determine the grouping form (5-4-2, 
4-4-2, etc.) from a 10-digit without dashes, the exact NDC cannot be 
determined.  The ambiguity therefore means that this approach may produce 
unrelated results.  Due to this possibility, this approach is disabled by
default for the barcode search endpoint (it may be enabled via a request 
parameter, and even then will only be used if an exact UPC match was not 
found.)  It is, however, enabled by default for the "all-in" search (but may 
be disabled by request parameter).

Note that this approach will only yield results if the dictionary service is
capable of dealing with partial NDCs.

## Errata

### SPI

Only one instance of a given implementation of `DictionaryProvider` may be 
handled by CRxREST.  The rationale for this is the same as for Java's native
SPI mechanism.

### Documentation

Due to a flaw in `swagger-core`, there appears to be no way to properly 
represent an empty body for those API operations that produce no content.  
According to the generated API spec, such operations appear to produce an
empty object.  In reality they do not.  Therefore, whenever an API operation
is documented as returning a 204 (No Content) status code, do not expect a
response body to be present.  None will.


(This has been [reported to the swagger-core developers](https://github.com/swagger-api/swagger-core/issues/2446), but as of this writing no fix has been provided.)
